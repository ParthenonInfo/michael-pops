﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.Folha_Pagamento
{
    public class VwConsultarFolhapagamento
    {
        public string Nome { get; set; }

        public string Departamento { get; set; }

        public string Mês { get; set; }

        public decimal SalarioLiquido { get; set; }

    }
}
