﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.Compras
{
   public   class ComprasDTO
    {
        public int id_compra { get; set; }
        public int id_fornecedor { get; set; }
        public DateTime dt_compra { get; set; }
        
    }
}
