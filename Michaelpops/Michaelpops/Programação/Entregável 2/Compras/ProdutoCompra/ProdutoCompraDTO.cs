﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.Entregável_2.Compras.ProdutoCompra
{
    public class ProdutoCompraDTO
    {
        public int id_produtocompra { get; set; }
        public string nm_produtocompra { get; set; }
        public decimal vl_valor { get; set; }
    }
}
