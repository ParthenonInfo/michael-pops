﻿using Michaelpops.Programação.Entregável_3.PedidoItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.PedidoItem
{
 public   class PedidoItemBusiness
    {
        PeditoItemDatabase db = new PeditoItemDatabase();

        public int Salvar(PedidoItemDTO pedidoitem)
        {
            return db.Salvar(pedidoitem);
        }

        public void Alterar(PedidoItemDTO pedidoitem)
        {

            db.Alterar(pedidoitem);
        }

        public List<PedidoItemDTO> Consultar(string pedidoitem)
        {
            return db.Consultar(pedidoitem);
        }

        public List<PedidoItemDTO> Listar()
        {
            return db.Listar();
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
        public List<VwConsultarItem> ListarItem()
        {
            return db.ListarItem();
        }
        public List<VwConsultarItem> ConsultarItem(int comandaid)
        {
            

            return db.ConsultarItem(comandaid);
        }
    }
}
