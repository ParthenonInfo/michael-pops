﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.PedidoItem
{
    public class PedidoItemDTO
    {
        public int id_pedidoitem { get; set; }
        public int id_produto { get; set; }
        public int id_pedido { get; set; }
    }
}
