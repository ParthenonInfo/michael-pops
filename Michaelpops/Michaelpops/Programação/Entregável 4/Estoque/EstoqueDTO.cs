﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Michaelpops.Programação.Estoque
{
  public  class EstoqueDTO
    {
        public int id_estoque { get; set; }

        public int id_produtocompra { get; set; }

        public decimal ds_quantidade { get; set; }
    }
}

