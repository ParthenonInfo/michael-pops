﻿using Michaelpops.Programação.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Michaelpops.Telas.API
{
    public partial class Email : Form
    {
        public Email()
        {
            InitializeComponent();
        }
        Email_Programa email = new Email_Programa();
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                email.Mensagem = txtMensagem.Text.Trim();
                email.Assunto = txtAssunto.Text.Trim();
                email.Para = txtPara.Text.Trim();

                email.Enviar();

                MessageBox.Show("Mensagem enviada com sucesso", "Michael Pop's");
                txtMensagem.Clear();
                txtAssunto.Clear();
                txtPara.Clear();
                button1.Text = "Anexar";
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Ocorreu o erro : " + Ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            janela.ShowDialog();

            button1.Text = janela.FileName;

            email.AdicionarAnexo(janela.FileName);
        }
    }
}
