﻿using Michaelpops.Programação;
using Michaelpops.Programação.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas.CRUD.Clientess
{
    public partial class AlterarClientes : Form
    {
        ClienteDTO cliente;
        Validação v = new Validação();

        public AlterarClientes()
        {
            InitializeComponent();
        }

        public void LoadScreen(ClienteDTO cliente)
        {
            this.cliente = cliente;
            txtnome.Text = cliente.nm_nome;
            txtsobrenome.Text = cliente.nm_sobrenome;
            txtcpf.Text = cliente.ds_cpf;
            dtpnascimento.Value = cliente.dt_datanascimento;
            txtemail.Text = cliente.ds_email;
                

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Cliente tela = new Cliente();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                cliente.nm_nome = txtnome.Text;
                cliente.nm_sobrenome = txtsobrenome.Text;
                cliente.ds_cpf = txtcpf.Text;
                cliente.dt_datanascimento = dtpnascimento.Value;
                cliente.ds_email = txtemail.Text;

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(cliente);
                MessageBox.Show("Cliente alterado com sucesso");

                Cliente tela = new Cliente();
                tela.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtcpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
