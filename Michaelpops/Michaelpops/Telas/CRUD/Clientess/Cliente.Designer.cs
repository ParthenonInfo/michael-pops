﻿namespace Michaelpops.Telas
{
    partial class Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cliente));
            this.tcCadastrarFuncionario = new System.Windows.Forms.TabControl();
            this.Cadastrar = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSobrenomeCliente = new System.Windows.Forms.TextBox();
            this.TxtNomeCliente = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.mtbCpfCliente = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpNascimentoCliente = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEmailCliente = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSalvarCliente = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnalterar = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.dgvConsultarClientes = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sobrenome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataNascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtConsultarCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnConsultarCliente = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tcCadastrarFuncionario.SuspendLayout();
            this.Cadastrar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tcCadastrarFuncionario
            // 
            this.tcCadastrarFuncionario.Controls.Add(this.Cadastrar);
            this.tcCadastrarFuncionario.Controls.Add(this.tabPage2);
            this.tcCadastrarFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCadastrarFuncionario.Location = new System.Drawing.Point(12, 12);
            this.tcCadastrarFuncionario.Name = "tcCadastrarFuncionario";
            this.tcCadastrarFuncionario.SelectedIndex = 0;
            this.tcCadastrarFuncionario.Size = new System.Drawing.Size(1006, 558);
            this.tcCadastrarFuncionario.TabIndex = 36;
            // 
            // Cadastrar
            // 
            this.Cadastrar.BackColor = System.Drawing.Color.Snow;
            this.Cadastrar.Controls.Add(this.groupBox1);
            this.Cadastrar.Controls.Add(this.pictureBox1);
            this.Cadastrar.Controls.Add(this.BtnSalvarCliente);
            this.Cadastrar.Controls.Add(this.label2);
            this.Cadastrar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Cadastrar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cadastrar.Location = new System.Drawing.Point(4, 26);
            this.Cadastrar.Name = "Cadastrar";
            this.Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.Cadastrar.Size = new System.Drawing.Size(998, 528);
            this.Cadastrar.TabIndex = 0;
            this.Cadastrar.Text = "Cadastrar";
            this.Cadastrar.Click += new System.EventHandler(this.Cadastrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtSobrenomeCliente);
            this.groupBox1.Controls.Add(this.TxtNomeCliente);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.mtbCpfCliente);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpNascimentoCliente);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtEmailCliente);
            this.groupBox1.Location = new System.Drawing.Point(21, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(358, 251);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nome";
            // 
            // TxtSobrenomeCliente
            // 
            this.TxtSobrenomeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSobrenomeCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSobrenomeCliente.Location = new System.Drawing.Point(114, 65);
            this.TxtSobrenomeCliente.MaxLength = 20;
            this.TxtSobrenomeCliente.Name = "TxtSobrenomeCliente";
            this.TxtSobrenomeCliente.Size = new System.Drawing.Size(190, 21);
            this.TxtSobrenomeCliente.TabIndex = 2;
            this.TxtSobrenomeCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeCliente_KeyPress);
            // 
            // TxtNomeCliente
            // 
            this.TxtNomeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNomeCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNomeCliente.Location = new System.Drawing.Point(114, 22);
            this.TxtNomeCliente.MaxLength = 20;
            this.TxtNomeCliente.Name = "TxtNomeCliente";
            this.TxtNomeCliente.Size = new System.Drawing.Size(190, 21);
            this.TxtNomeCliente.TabIndex = 1;
            this.TxtNomeCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeCliente_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(5, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 42);
            this.label15.TabIndex = 17;
            this.label15.Text = "Data de \r\nNascimento";
            // 
            // mtbCpfCliente
            // 
            this.mtbCpfCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCpfCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCpfCliente.Location = new System.Drawing.Point(114, 110);
            this.mtbCpfCliente.Mask = "000.000.000-00";
            this.mtbCpfCliente.Name = "mtbCpfCliente";
            this.mtbCpfCliente.Size = new System.Drawing.Size(190, 21);
            this.mtbCpfCliente.TabIndex = 3;
            this.mtbCpfCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCpfCliente_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(70, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 21);
            this.label6.TabIndex = 8;
            this.label6.Text = "Cpf";
            // 
            // dtpNascimentoCliente
            // 
            this.dtpNascimentoCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimentoCliente.Location = new System.Drawing.Point(115, 158);
            this.dtpNascimentoCliente.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpNascimentoCliente.MinDate = new System.DateTime(1910, 1, 1, 0, 0, 0, 0);
            this.dtpNascimentoCliente.Name = "dtpNascimentoCliente";
            this.dtpNascimentoCliente.Size = new System.Drawing.Size(190, 21);
            this.dtpNascimentoCliente.TabIndex = 4;
            this.dtpNascimentoCliente.ValueChanged += new System.EventHandler(this.dtpNascimentoCliente_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sobrenome";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(58, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 21);
            this.label9.TabIndex = 11;
            this.label9.Text = "Email";
            // 
            // txtEmailCliente
            // 
            this.txtEmailCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailCliente.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailCliente.Location = new System.Drawing.Point(115, 208);
            this.txtEmailCliente.MaxLength = 50;
            this.txtEmailCliente.Name = "txtEmailCliente";
            this.txtEmailCliente.Size = new System.Drawing.Size(190, 21);
            this.txtEmailCliente.TabIndex = 5;
            this.txtEmailCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeCliente_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(926, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // BtnSalvarCliente
            // 
            this.BtnSalvarCliente.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalvarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSalvarCliente.FlatAppearance.BorderSize = 0;
            this.BtnSalvarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSalvarCliente.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalvarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalvarCliente.Image")));
            this.BtnSalvarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSalvarCliente.Location = new System.Drawing.Point(870, 483);
            this.BtnSalvarCliente.Name = "BtnSalvarCliente";
            this.BtnSalvarCliente.Size = new System.Drawing.Size(122, 39);
            this.BtnSalvarCliente.TabIndex = 2;
            this.BtnSalvarCliente.Text = "Salvar";
            this.BtnSalvarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSalvarCliente.UseVisualStyleBackColor = false;
            this.BtnSalvarCliente.Click += new System.EventHandler(this.BtnSalvarCliente_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 36);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cliente";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Snow;
            this.tabPage2.Controls.Add(this.btnalterar);
            this.tabPage2.Controls.Add(this.btnApagar);
            this.tabPage2.Controls.Add(this.dgvConsultarClientes);
            this.tabPage2.Controls.Add(this.txtConsultarCliente);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.BtnConsultarCliente);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabPage2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(998, 528);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consultar/Apagar/Alterar";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // btnalterar
            // 
            this.btnalterar.BackColor = System.Drawing.Color.Transparent;
            this.btnalterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnalterar.FlatAppearance.BorderSize = 0;
            this.btnalterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnalterar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnalterar.Image = ((System.Drawing.Image)(resources.GetObject("btnalterar.Image")));
            this.btnalterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnalterar.Location = new System.Drawing.Point(764, 43);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(119, 39);
            this.btnalterar.TabIndex = 21;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnalterar.UseVisualStyleBackColor = false;
            this.btnalterar.Click += new System.EventHandler(this.btnalterar_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.BackColor = System.Drawing.Color.Transparent;
            this.btnApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApagar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApagar.Location = new System.Drawing.Point(643, 42);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(115, 39);
            this.btnApagar.TabIndex = 20;
            this.btnApagar.Text = "Apagar";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // dgvConsultarClientes
            // 
            this.dgvConsultarClientes.AllowUserToAddRows = false;
            this.dgvConsultarClientes.AllowUserToDeleteRows = false;
            this.dgvConsultarClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Sobrenome,
            this.Cpf,
            this.DataNascimento,
            this.Email});
            this.dgvConsultarClientes.Location = new System.Drawing.Point(21, 88);
            this.dgvConsultarClientes.Name = "dgvConsultarClientes";
            this.dgvConsultarClientes.ReadOnly = true;
            this.dgvConsultarClientes.RowHeadersVisible = false;
            this.dgvConsultarClientes.Size = new System.Drawing.Size(960, 434);
            this.dgvConsultarClientes.TabIndex = 17;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "nm_nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Sobrenome
            // 
            this.Sobrenome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Sobrenome.DataPropertyName = "nm_sobrenome";
            this.Sobrenome.HeaderText = "Sobrenome";
            this.Sobrenome.Name = "Sobrenome";
            this.Sobrenome.ReadOnly = true;
            // 
            // Cpf
            // 
            this.Cpf.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Cpf.DataPropertyName = "ds_cpf";
            this.Cpf.HeaderText = "Cpf";
            this.Cpf.Name = "Cpf";
            this.Cpf.ReadOnly = true;
            // 
            // DataNascimento
            // 
            this.DataNascimento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DataNascimento.DataPropertyName = "dt_datanascimento";
            this.DataNascimento.HeaderText = "DataNascimento";
            this.DataNascimento.Name = "DataNascimento";
            this.DataNascimento.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Email.DataPropertyName = "ds_email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // txtConsultarCliente
            // 
            this.txtConsultarCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConsultarCliente.Location = new System.Drawing.Point(21, 59);
            this.txtConsultarCliente.MaxLength = 50;
            this.txtConsultarCliente.Name = "txtConsultarCliente";
            this.txtConsultarCliente.Size = new System.Drawing.Size(479, 23);
            this.txtConsultarCliente.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "Cliente";
            // 
            // BtnConsultarCliente
            // 
            this.BtnConsultarCliente.BackColor = System.Drawing.Color.Transparent;
            this.BtnConsultarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnConsultarCliente.FlatAppearance.BorderSize = 0;
            this.BtnConsultarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnConsultarCliente.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConsultarCliente.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultarCliente.Image")));
            this.BtnConsultarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnConsultarCliente.Location = new System.Drawing.Point(506, 43);
            this.BtnConsultarCliente.Name = "BtnConsultarCliente";
            this.BtnConsultarCliente.Size = new System.Drawing.Size(131, 39);
            this.BtnConsultarCliente.TabIndex = 5;
            this.BtnConsultarCliente.Text = "Consultar";
            this.BtnConsultarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnConsultarCliente.UseVisualStyleBackColor = false;
            this.BtnConsultarCliente.Click += new System.EventHandler(this.BtnConsultarCliente_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(926, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(1030, 592);
            this.Controls.Add(this.tcCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroClientes";
            this.Load += new System.EventHandler(this.Cliente_Load);
            this.tcCadastrarFuncionario.ResumeLayout(false);
            this.Cadastrar.ResumeLayout(false);
            this.Cadastrar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcCadastrarFuncionario;
        private System.Windows.Forms.TabPage Cadastrar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnSalvarCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvConsultarClientes;
        private System.Windows.Forms.TextBox txtConsultarCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnConsultarCliente;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtSobrenomeCliente;
        private System.Windows.Forms.TextBox TxtNomeCliente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox mtbCpfCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpNascimentoCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmailCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sobrenome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataNascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.Button btnApagar;
    }
}