﻿using Michaelpops.Programação;
using Michaelpops.Programação.Cliente;
using Michaelpops.Programação.Funcionario;
using Michaelpops.Telas.CRUD.Clientess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class Cliente : Form
    {
        Validação v = new Validação();
        public Cliente()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void Cliente_Load(object sender, EventArgs e)
        {

        }

        private void Cadastrar_Click(object sender, EventArgs e)
        {

        }

        private void TxtNomeCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void mtbCpfCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void dtpNascimentoCliente_ValueChanged(object sender, EventArgs e)
        {

        }

        private void BtnSalvarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.nm_nome = TxtNomeCliente.Text;
                dto.nm_sobrenome = TxtSobrenomeCliente.Text;
                dto.ds_cpf = mtbCpfCliente.Text;
                dto.dt_datanascimento = dtpNascimentoCliente.Value;
                dto.ds_email = txtEmailCliente.Text;

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(dto);

                MessageBox.Show("Cliente salvo com sucesso.");
                TxtNomeCliente.Text = "";
                TxtSobrenomeCliente.Text = "";
                mtbCpfCliente.Text = "";
                txtEmailCliente.Text = "";

            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void BtnConsultarCliente_Click(object sender, EventArgs e)
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> a = business.Consultar(txtConsultarCliente.Text);
            dgvConsultarClientes.AutoGenerateColumns = false;
            dgvConsultarClientes.DataSource = a;
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgvConsultarClientes.CurrentRow != null)
                {
                    ClienteDTO cliente = dgvConsultarClientes.CurrentRow.DataBoundItem as ClienteDTO;

                    AlterarClientes tela = new AlterarClientes();
                    tela.LoadScreen(cliente);
                    tela.Show();
                    this.Hide();
                }
               else
                {
                    MessageBox.Show("Selecione um cliente");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgvConsultarClientes.CurrentRow != null)
                {
                    ClienteDTO cliente = dgvConsultarClientes.CurrentRow.DataBoundItem as ClienteDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse cliente?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        ClienteBusiness business = new ClienteBusiness();
                        business.Remover(cliente.id_cliente);

                        List<ClienteDTO> a = business.Consultar(txtConsultarCliente.Text);
                        dgvConsultarClientes.AutoGenerateColumns = false;
                        dgvConsultarClientes.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um cliente");
                    }

                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }
    }
}
