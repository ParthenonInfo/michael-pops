﻿using Michaelpops.Programação;
using Michaelpops.Programação.Entregável_2.Compras;
using Michaelpops.Programação.Entregável_2.Compras.ProdutoCompra;
using Michaelpops.Programação.Entregável_4.Estoque;
using Michaelpops.Programação.Estoque;
using Michaelpops.Programação.Produto;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class Estoque : Form
    {
        Validação V = new Validação();
        public Estoque()
        {
            InitializeComponent();
            CarregarCombos();
        

        }
        
        void CarregarCombos()
        {
            ProdutoCompraBusiness busi = new ProdutoCompraBusiness();
            List<Programação.Entregável_2.Compras.ProdutoCompra.ProdutoCompraDTO> list = busi.Consultar(string.Empty);
            cbProduto.DisplayMember = nameof(ProdutoCompraDTO.nm_produtocompra);
            cbProduto.ValueMember = nameof(ProdutoCompraDTO.id_produtocompra);
            cbProduto.DataSource = list;

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void txtValorCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }

        private void TxtqtdProdutos_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
          
        }

        private void Cadastrar_Click(object sender, EventArgs e)
        {

        }

        private void BtnSalvarEstoque_Click(object sender, EventArgs e)
        {

        }

        private void BtnConsultarEstoque_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                List<vwconsultarestoque> a = business.ConsultarEstoque(txtConsultarestoque.Text);
                dgvconsultaestoq.AutoGenerateColumns = false;
                dgvconsultaestoq.DataSource = a;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void BtnSalvarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if(lblAtual.Text == "0")
                {
                    MessageBox.Show("Pesquise o Produto de compra");
                }
                if(nudQtd.Value == 0)
                {
                    MessageBox.Show("Coloque o valor para retirar");
                }
                
                else
                {
                    ProdutoCompraDTO dto = cbProduto.SelectedItem as ProdutoCompraDTO;
                    EstoqueDTO estoque = new EstoqueDTO();
                    EstoqueBusiness business = new EstoqueBusiness();
                    estoque.id_produtocompra = dto.id_produtocompra;
                    estoque.ds_quantidade = Convert.ToDecimal(lblAtual.Text) - nudQtd.Value;
                    business.Alterar(estoque);
                    MessageBox.Show("Estoque alterado com sucesso");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void lblAtual_Click(object sender, EventArgs e)
        {
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cbProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoCompraDTO dto = new ProdutoCompraDTO();
            EstoqueBusiness business = new EstoqueBusiness();
            vwconsultarestoque view = business.ConsultarEstoqueView(dto.nm_produtocompra);
            lblAtual.Text = view.ds_quantidade.ToString();
        }

        private void cbProduto_ValueMemberChanged(object sender, EventArgs e)
        {








        }

        private void tcCadastrarFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
          




        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
           
        }

        private void cbProduto_DisplayMemberChanged(object sender, EventArgs e)
        {
           
        }

        private void Pesquisar_Click(object sender, EventArgs e)
        {
            ProdutoCompraDTO dto = cbProduto.SelectedItem as ProdutoCompraDTO;
            EstoqueBusiness business = new EstoqueBusiness();
            vwconsultarestoque view = business.ConsultarEstoqueView(dto.nm_produtocompra);
            lblAtual.Text = view.ds_quantidade.ToString();
        }
    }
}
