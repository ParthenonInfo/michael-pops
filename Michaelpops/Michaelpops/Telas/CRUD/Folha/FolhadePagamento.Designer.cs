﻿namespace Michaelpops.Telas
{
    partial class FolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolhadePagamento));
            this.tcCadastrarFuncionario = new System.Windows.Forms.TabControl();
            this.Cadastrar = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbmes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lblSalarioLiquido = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.txtFaltaQuarSemana = new System.Windows.Forms.TextBox();
            this.txtFaltaTerSemana = new System.Windows.Forms.TextBox();
            this.txtFaltaSegSemana = new System.Windows.Forms.TextBox();
            this.txtFaltasPriSemana = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkHoraExtra = new System.Windows.Forms.CheckBox();
            this.lblPorcentagem = new System.Windows.Forms.Label();
            this.nudPorcentagem = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudHorasTrabalhadas = new System.Windows.Forms.NumericUpDown();
            this.lblHoraExtra = new System.Windows.Forms.Label();
            this.nudHorasExtras = new System.Windows.Forms.NumericUpDown();
            this.chkVT = new System.Windows.Forms.CheckBox();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSalvarFolha = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cmbmes = new System.Windows.Forms.ComboBox();
            this.dgvConsultarFolha = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Departamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mês = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalárioLíquido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnConsultarFolha = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.tcCadastrarFuncionario.SuspendLayout();
            this.Cadastrar.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasTrabalhadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFolha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // tcCadastrarFuncionario
            // 
            this.tcCadastrarFuncionario.Controls.Add(this.Cadastrar);
            this.tcCadastrarFuncionario.Controls.Add(this.tabPage3);
            this.tcCadastrarFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCadastrarFuncionario.Location = new System.Drawing.Point(12, 17);
            this.tcCadastrarFuncionario.Name = "tcCadastrarFuncionario";
            this.tcCadastrarFuncionario.SelectedIndex = 0;
            this.tcCadastrarFuncionario.Size = new System.Drawing.Size(1006, 558);
            this.tcCadastrarFuncionario.TabIndex = 38;
            // 
            // Cadastrar
            // 
            this.Cadastrar.BackColor = System.Drawing.Color.Snow;
            this.Cadastrar.Controls.Add(this.groupBox2);
            this.Cadastrar.Controls.Add(this.pictureBox1);
            this.Cadastrar.Controls.Add(this.BtnSalvarFolha);
            this.Cadastrar.Controls.Add(this.label2);
            this.Cadastrar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Cadastrar.Location = new System.Drawing.Point(4, 26);
            this.Cadastrar.Name = "Cadastrar";
            this.Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.Cadastrar.Size = new System.Drawing.Size(998, 528);
            this.Cadastrar.TabIndex = 0;
            this.Cadastrar.Text = "Folha Pagamento";
            this.Cadastrar.Click += new System.EventHandler(this.Cadastrar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbmes);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnCalcular);
            this.groupBox2.Controls.Add(this.lblSalarioLiquido);
            this.groupBox2.Controls.Add(this.lblText);
            this.groupBox2.Controls.Add(this.txtFaltaQuarSemana);
            this.groupBox2.Controls.Add(this.txtFaltaTerSemana);
            this.groupBox2.Controls.Add(this.txtFaltaSegSemana);
            this.groupBox2.Controls.Add(this.txtFaltasPriSemana);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.chkHoraExtra);
            this.groupBox2.Controls.Add(this.lblPorcentagem);
            this.groupBox2.Controls.Add(this.nudPorcentagem);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.nudHorasTrabalhadas);
            this.groupBox2.Controls.Add(this.lblHoraExtra);
            this.groupBox2.Controls.Add(this.nudHorasExtras);
            this.groupBox2.Controls.Add(this.chkVT);
            this.groupBox2.Controls.Add(this.cboFuncionario);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(21, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(435, 466);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            // 
            // cbmes
            // 
            this.cbmes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbmes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbmes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbmes.FormattingEnabled = true;
            this.cbmes.Items.AddRange(new object[] {
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"});
            this.cbmes.Location = new System.Drawing.Point(191, 24);
            this.cbmes.Name = "cbmes";
            this.cbmes.Size = new System.Drawing.Size(188, 25);
            this.cbmes.TabIndex = 1;
            this.cbmes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbmes_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(136, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 40;
            this.label1.Text = "Mês :";
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Transparent;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.ForeColor = System.Drawing.Color.Black;
            this.btnCalcular.Location = new System.Drawing.Point(13, 412);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(116, 36);
            this.btnCalcular.TabIndex = 12;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // lblSalarioLiquido
            // 
            this.lblSalarioLiquido.AutoSize = true;
            this.lblSalarioLiquido.BackColor = System.Drawing.Color.Transparent;
            this.lblSalarioLiquido.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalarioLiquido.ForeColor = System.Drawing.Color.Black;
            this.lblSalarioLiquido.Location = new System.Drawing.Point(198, 376);
            this.lblSalarioLiquido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSalarioLiquido.Name = "lblSalarioLiquido";
            this.lblSalarioLiquido.Size = new System.Drawing.Size(12, 17);
            this.lblSalarioLiquido.TabIndex = 38;
            this.lblSalarioLiquido.Text = "-";
            this.lblSalarioLiquido.Click += new System.EventHandler(this.lblSalarioLiquido_Click);
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.BackColor = System.Drawing.Color.Transparent;
            this.lblText.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.ForeColor = System.Drawing.Color.Black;
            this.lblText.Location = new System.Drawing.Point(72, 376);
            this.lblText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(110, 17);
            this.lblText.TabIndex = 37;
            this.lblText.Text = "Salário Líquido :";
            // 
            // txtFaltaQuarSemana
            // 
            this.txtFaltaQuarSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaQuarSemana.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaltaQuarSemana.Location = new System.Drawing.Point(188, 208);
            this.txtFaltaQuarSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaQuarSemana.MaxLength = 1;
            this.txtFaltaQuarSemana.Name = "txtFaltaQuarSemana";
            this.txtFaltaQuarSemana.Size = new System.Drawing.Size(191, 23);
            this.txtFaltaQuarSemana.TabIndex = 7;
            // 
            // txtFaltaTerSemana
            // 
            this.txtFaltaTerSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaTerSemana.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaltaTerSemana.Location = new System.Drawing.Point(188, 178);
            this.txtFaltaTerSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaTerSemana.MaxLength = 1;
            this.txtFaltaTerSemana.Name = "txtFaltaTerSemana";
            this.txtFaltaTerSemana.Size = new System.Drawing.Size(191, 23);
            this.txtFaltaTerSemana.TabIndex = 6;
            // 
            // txtFaltaSegSemana
            // 
            this.txtFaltaSegSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltaSegSemana.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaltaSegSemana.Location = new System.Drawing.Point(188, 148);
            this.txtFaltaSegSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltaSegSemana.MaxLength = 1;
            this.txtFaltaSegSemana.Name = "txtFaltaSegSemana";
            this.txtFaltaSegSemana.Size = new System.Drawing.Size(191, 23);
            this.txtFaltaSegSemana.TabIndex = 5;
            // 
            // txtFaltasPriSemana
            // 
            this.txtFaltasPriSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaltasPriSemana.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaltasPriSemana.Location = new System.Drawing.Point(189, 118);
            this.txtFaltasPriSemana.Margin = new System.Windows.Forms.Padding(4);
            this.txtFaltasPriSemana.MaxLength = 1;
            this.txtFaltasPriSemana.Name = "txtFaltasPriSemana";
            this.txtFaltasPriSemana.Size = new System.Drawing.Size(190, 23);
            this.txtFaltasPriSemana.TabIndex = 4;
            this.txtFaltasPriSemana.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFaltasPriSemana_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(14, 210);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Faltas na quarta semana :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(7, 180);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 17);
            this.label6.TabIndex = 35;
            this.label6.Text = "Faltas na terceira semana :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(-1, 150);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 17);
            this.label4.TabIndex = 34;
            this.label4.Text = "Faltas na segunda semana :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(3, 124);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(187, 17);
            this.label7.TabIndex = 33;
            this.label7.Text = "Faltas na primeira semana :";
            // 
            // chkHoraExtra
            // 
            this.chkHoraExtra.AutoSize = true;
            this.chkHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.chkHoraExtra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkHoraExtra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHoraExtra.ForeColor = System.Drawing.Color.Black;
            this.chkHoraExtra.Location = new System.Drawing.Point(270, 252);
            this.chkHoraExtra.Margin = new System.Windows.Forms.Padding(4);
            this.chkHoraExtra.Name = "chkHoraExtra";
            this.chkHoraExtra.Size = new System.Drawing.Size(91, 21);
            this.chkHoraExtra.TabIndex = 9;
            this.chkHoraExtra.Text = "Hora extra";
            this.chkHoraExtra.UseVisualStyleBackColor = false;
            this.chkHoraExtra.CheckedChanged += new System.EventHandler(this.chkHoraExtra_CheckedChanged);
            // 
            // lblPorcentagem
            // 
            this.lblPorcentagem.AutoSize = true;
            this.lblPorcentagem.BackColor = System.Drawing.Color.Transparent;
            this.lblPorcentagem.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentagem.ForeColor = System.Drawing.Color.Black;
            this.lblPorcentagem.Location = new System.Drawing.Point(81, 341);
            this.lblPorcentagem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPorcentagem.Name = "lblPorcentagem";
            this.lblPorcentagem.Size = new System.Drawing.Size(105, 17);
            this.lblPorcentagem.TabIndex = 31;
            this.lblPorcentagem.Text = "Porcentagem :";
            // 
            // nudPorcentagem
            // 
            this.nudPorcentagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudPorcentagem.DecimalPlaces = 2;
            this.nudPorcentagem.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPorcentagem.Location = new System.Drawing.Point(188, 335);
            this.nudPorcentagem.Margin = new System.Windows.Forms.Padding(4);
            this.nudPorcentagem.Name = "nudPorcentagem";
            this.nudPorcentagem.Size = new System.Drawing.Size(191, 23);
            this.nudPorcentagem.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(44, 94);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 17);
            this.label8.TabIndex = 29;
            this.label8.Text = "Horas Trabalhadas :";
            // 
            // nudHorasTrabalhadas
            // 
            this.nudHorasTrabalhadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasTrabalhadas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudHorasTrabalhadas.Location = new System.Drawing.Point(188, 88);
            this.nudHorasTrabalhadas.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasTrabalhadas.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudHorasTrabalhadas.Name = "nudHorasTrabalhadas";
            this.nudHorasTrabalhadas.Size = new System.Drawing.Size(191, 23);
            this.nudHorasTrabalhadas.TabIndex = 3;
            this.nudHorasTrabalhadas.ValueChanged += new System.EventHandler(this.nudHorasTrabalhadas_ValueChanged);
            // 
            // lblHoraExtra
            // 
            this.lblHoraExtra.AutoSize = true;
            this.lblHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.lblHoraExtra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraExtra.ForeColor = System.Drawing.Color.Black;
            this.lblHoraExtra.Location = new System.Drawing.Point(89, 303);
            this.lblHoraExtra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHoraExtra.Name = "lblHoraExtra";
            this.lblHoraExtra.Size = new System.Drawing.Size(92, 17);
            this.lblHoraExtra.TabIndex = 27;
            this.lblHoraExtra.Text = "Horas Extras :";
            // 
            // nudHorasExtras
            // 
            this.nudHorasExtras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasExtras.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudHorasExtras.Location = new System.Drawing.Point(188, 297);
            this.nudHorasExtras.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasExtras.Name = "nudHorasExtras";
            this.nudHorasExtras.Size = new System.Drawing.Size(191, 23);
            this.nudHorasExtras.TabIndex = 10;
            // 
            // chkVT
            // 
            this.chkVT.AutoSize = true;
            this.chkVT.BackColor = System.Drawing.Color.Transparent;
            this.chkVT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkVT.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVT.ForeColor = System.Drawing.Color.Black;
            this.chkVT.Location = new System.Drawing.Point(95, 253);
            this.chkVT.Margin = new System.Windows.Forms.Padding(4);
            this.chkVT.Name = "chkVT";
            this.chkVT.Size = new System.Drawing.Size(123, 21);
            this.chkVT.TabIndex = 8;
            this.chkVT.Text = "Vale transporte";
            this.chkVT.UseVisualStyleBackColor = false;
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFuncionario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(188, 56);
            this.cboFuncionario.Margin = new System.Windows.Forms.Padding(4);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(191, 25);
            this.cboFuncionario.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(92, 64);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "Funcionário :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(926, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // BtnSalvarFolha
            // 
            this.BtnSalvarFolha.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalvarFolha.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSalvarFolha.FlatAppearance.BorderSize = 0;
            this.BtnSalvarFolha.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSalvarFolha.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalvarFolha.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalvarFolha.Image")));
            this.BtnSalvarFolha.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSalvarFolha.Location = new System.Drawing.Point(870, 483);
            this.BtnSalvarFolha.Name = "BtnSalvarFolha";
            this.BtnSalvarFolha.Size = new System.Drawing.Size(122, 39);
            this.BtnSalvarFolha.TabIndex = 13;
            this.BtnSalvarFolha.Text = "Salvar";
            this.BtnSalvarFolha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSalvarFolha.UseVisualStyleBackColor = false;
            this.BtnSalvarFolha.Click += new System.EventHandler(this.BtnSalvarEstoque_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(318, 36);
            this.label2.TabIndex = 4;
            this.label2.Text = "Folha de Pagamento";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Snow;
            this.tabPage3.Controls.Add(this.cmbmes);
            this.tabPage3.Controls.Add(this.dgvConsultarFolha);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.BtnConsultarFolha);
            this.tabPage3.Controls.Add(this.pictureBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(998, 528);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Consultar";
            // 
            // cmbmes
            // 
            this.cmbmes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbmes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbmes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbmes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbmes.FormattingEnabled = true;
            this.cmbmes.Items.AddRange(new object[] {
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"});
            this.cmbmes.Location = new System.Drawing.Point(20, 58);
            this.cmbmes.Name = "cmbmes";
            this.cmbmes.Size = new System.Drawing.Size(479, 25);
            this.cmbmes.TabIndex = 1;
            // 
            // dgvConsultarFolha
            // 
            this.dgvConsultarFolha.AllowUserToAddRows = false;
            this.dgvConsultarFolha.AllowUserToDeleteRows = false;
            this.dgvConsultarFolha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarFolha.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Departamento,
            this.Mês,
            this.SalárioLíquido});
            this.dgvConsultarFolha.Location = new System.Drawing.Point(20, 87);
            this.dgvConsultarFolha.Name = "dgvConsultarFolha";
            this.dgvConsultarFolha.ReadOnly = true;
            this.dgvConsultarFolha.RowHeadersVisible = false;
            this.dgvConsultarFolha.Size = new System.Drawing.Size(960, 434);
            this.dgvConsultarFolha.TabIndex = 54;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Departamento
            // 
            this.Departamento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Departamento.DataPropertyName = "Departamento";
            this.Departamento.HeaderText = "Departamento";
            this.Departamento.Name = "Departamento";
            this.Departamento.ReadOnly = true;
            // 
            // Mês
            // 
            this.Mês.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mês.DataPropertyName = "Mês";
            this.Mês.HeaderText = "Mês";
            this.Mês.Name = "Mês";
            this.Mês.ReadOnly = true;
            // 
            // SalárioLíquido
            // 
            this.SalárioLíquido.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SalárioLíquido.DataPropertyName = "SalarioLiquido";
            this.SalárioLíquido.HeaderText = "Salário Líquido";
            this.SalárioLíquido.Name = "SalárioLíquido";
            this.SalárioLíquido.ReadOnly = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(318, 36);
            this.label10.TabIndex = 52;
            this.label10.Text = "Folha de Pagamento";
            // 
            // BtnConsultarFolha
            // 
            this.BtnConsultarFolha.BackColor = System.Drawing.Color.Transparent;
            this.BtnConsultarFolha.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnConsultarFolha.FlatAppearance.BorderSize = 0;
            this.BtnConsultarFolha.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnConsultarFolha.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConsultarFolha.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultarFolha.Image")));
            this.BtnConsultarFolha.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnConsultarFolha.Location = new System.Drawing.Point(505, 42);
            this.BtnConsultarFolha.Name = "BtnConsultarFolha";
            this.BtnConsultarFolha.Size = new System.Drawing.Size(131, 39);
            this.BtnConsultarFolha.TabIndex = 2;
            this.BtnConsultarFolha.Text = "Consultar";
            this.BtnConsultarFolha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnConsultarFolha.UseVisualStyleBackColor = false;
            this.BtnConsultarFolha.Click += new System.EventHandler(this.BtnConsultarFolha_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(926, 6);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(66, 51);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 50;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(11, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(132, 21);
            this.label25.TabIndex = 6;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(9, 63);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(141, 20);
            this.label24.TabIndex = 17;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(154, 142);
            this.textBox8.MaxLength = 20;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(190, 20);
            this.textBox8.TabIndex = 4;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(11, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 21);
            this.label23.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(9, 103);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(114, 20);
            this.label22.TabIndex = 20;
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(154, 25);
            this.textBox7.MaxLength = 20;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(190, 20);
            this.textBox7.TabIndex = 1;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(154, 65);
            this.textBox6.MaxLength = 20;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(190, 20);
            this.textBox6.TabIndex = 2;
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(154, 102);
            this.textBox5.MaxLength = 20;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(190, 20);
            this.textBox5.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 178);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(86, 21);
            this.label21.TabIndex = 25;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(154, 181);
            this.textBox4.MaxLength = 20;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(190, 20);
            this.textBox4.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(9, 219);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 21);
            this.label20.TabIndex = 27;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(154, 222);
            this.textBox3.MaxLength = 20;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(190, 20);
            this.textBox3.TabIndex = 6;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(9, 254);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 21);
            this.label19.TabIndex = 29;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(154, 257);
            this.textBox2.MaxLength = 20;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(190, 20);
            this.textBox2.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 297);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 42);
            this.label18.TabIndex = 31;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(154, 300);
            this.textBox1.MaxLength = 20;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 20);
            this.textBox1.TabIndex = 8;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(11, 136);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(94, 42);
            this.label33.TabIndex = 6;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(9, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(129, 20);
            this.label32.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(11, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 21);
            this.label31.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(9, 103);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 20);
            this.label30.TabIndex = 20;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(11, 198);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 42);
            this.label29.TabIndex = 25;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(11, 257);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 21);
            this.label28.TabIndex = 27;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(11, 300);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(138, 21);
            this.label27.TabIndex = 29;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(175, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // textBox14
            // 
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(175, 65);
            this.textBox14.MaxLength = 20;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(200, 20);
            this.textBox14.TabIndex = 10;
            // 
            // textBox13
            // 
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(175, 105);
            this.textBox13.MaxLength = 20;
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(200, 20);
            this.textBox13.TabIndex = 11;
            // 
            // textBox12
            // 
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(175, 157);
            this.textBox12.MaxLength = 20;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(200, 20);
            this.textBox12.TabIndex = 12;
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(175, 201);
            this.textBox11.MaxLength = 20;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(200, 20);
            this.textBox11.TabIndex = 13;
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(175, 257);
            this.textBox10.MaxLength = 20;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(200, 20);
            this.textBox10.TabIndex = 14;
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(175, 303);
            this.textBox9.MaxLength = 20;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(200, 20);
            this.textBox9.TabIndex = 15;
            // 
            // FolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(1030, 592);
            this.Controls.Add(this.tcCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(140, 72);
            this.Name = "FolhadePagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FolhadePagamento";
            this.tcCadastrarFuncionario.ResumeLayout(false);
            this.Cadastrar.ResumeLayout(false);
            this.Cadastrar.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasTrabalhadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFolha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcCadastrarFuncionario;
        private System.Windows.Forms.TabPage Cadastrar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnSalvarFolha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox txtFaltaQuarSemana;
        private System.Windows.Forms.TextBox txtFaltaTerSemana;
        private System.Windows.Forms.TextBox txtFaltaSegSemana;
        private System.Windows.Forms.TextBox txtFaltasPriSemana;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkHoraExtra;
        private System.Windows.Forms.Label lblPorcentagem;
        private System.Windows.Forms.NumericUpDown nudPorcentagem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudHorasTrabalhadas;
        private System.Windows.Forms.Label lblHoraExtra;
        private System.Windows.Forms.NumericUpDown nudHorasExtras;
        private System.Windows.Forms.CheckBox chkVT;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lblSalarioLiquido;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.DataGridView dgvConsultarFolha;
        private System.Windows.Forms.Button BtnConsultarFolha;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbmes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Departamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mês;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalárioLíquido;
        private System.Windows.Forms.ComboBox cmbmes;
    }
}