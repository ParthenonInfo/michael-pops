﻿namespace Michaelpops.Telas
{
    partial class Fornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fornecedor));
            this.tcCadastrarFuncionario = new System.Windows.Forms.TabControl();
            this.Cadastrar = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mtbCnpjFornece = new System.Windows.Forms.MaskedTextBox();
            this.mtbTelefoneFornecedor = new System.Windows.Forms.MaskedTextBox();
            this.mtbCepFornece = new System.Windows.Forms.MaskedTextBox();
            this.txtemailFornece = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtnumeroFornece = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtComplementoFornece = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNomeFornec = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSalvarFornecedor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnalterar = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dgvConsultarFornecedor = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtConsultarFornecedor = new System.Windows.Forms.TextBox();
            this.btnconsultarFornecedor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tcCadastrarFuncionario.SuspendLayout();
            this.Cadastrar.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // tcCadastrarFuncionario
            // 
            this.tcCadastrarFuncionario.Controls.Add(this.Cadastrar);
            this.tcCadastrarFuncionario.Controls.Add(this.tabPage1);
            this.tcCadastrarFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCadastrarFuncionario.Location = new System.Drawing.Point(12, 17);
            this.tcCadastrarFuncionario.Name = "tcCadastrarFuncionario";
            this.tcCadastrarFuncionario.SelectedIndex = 0;
            this.tcCadastrarFuncionario.Size = new System.Drawing.Size(1006, 558);
            this.tcCadastrarFuncionario.TabIndex = 39;
            // 
            // Cadastrar
            // 
            this.Cadastrar.BackColor = System.Drawing.Color.Snow;
            this.Cadastrar.Controls.Add(this.groupBox2);
            this.Cadastrar.Controls.Add(this.pictureBox1);
            this.Cadastrar.Controls.Add(this.BtnSalvarFornecedor);
            this.Cadastrar.Controls.Add(this.label2);
            this.Cadastrar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Cadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cadastrar.Location = new System.Drawing.Point(4, 26);
            this.Cadastrar.Name = "Cadastrar";
            this.Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.Cadastrar.Size = new System.Drawing.Size(998, 528);
            this.Cadastrar.TabIndex = 0;
            this.Cadastrar.Text = "Cadastrar";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mtbCnpjFornece);
            this.groupBox2.Controls.Add(this.mtbTelefoneFornecedor);
            this.groupBox2.Controls.Add(this.mtbCepFornece);
            this.groupBox2.Controls.Add(this.txtemailFornece);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtnumeroFornece);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtComplementoFornece);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtNomeFornec);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(21, 60);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(358, 300);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // mtbCnpjFornece
            // 
            this.mtbCnpjFornece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCnpjFornece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCnpjFornece.Location = new System.Drawing.Point(135, 62);
            this.mtbCnpjFornece.Mask = "99.999.999/9999-99";
            this.mtbCnpjFornece.Name = "mtbCnpjFornece";
            this.mtbCnpjFornece.Size = new System.Drawing.Size(190, 20);
            this.mtbCnpjFornece.TabIndex = 2;
            this.mtbCnpjFornece.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCnpjFornece_KeyPress);
            // 
            // mtbTelefoneFornecedor
            // 
            this.mtbTelefoneFornecedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbTelefoneFornecedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbTelefoneFornecedor.Location = new System.Drawing.Point(135, 102);
            this.mtbTelefoneFornecedor.Mask = "(00) 0000-0000";
            this.mtbTelefoneFornecedor.Name = "mtbTelefoneFornecedor";
            this.mtbTelefoneFornecedor.Size = new System.Drawing.Size(190, 20);
            this.mtbTelefoneFornecedor.TabIndex = 3;
            this.mtbTelefoneFornecedor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCnpjFornece_KeyPress);
            // 
            // mtbCepFornece
            // 
            this.mtbCepFornece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCepFornece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCepFornece.Location = new System.Drawing.Point(135, 136);
            this.mtbCepFornece.Mask = "00000-000";
            this.mtbCepFornece.Name = "mtbCepFornece";
            this.mtbCepFornece.Size = new System.Drawing.Size(190, 20);
            this.mtbCepFornece.TabIndex = 4;
            this.mtbCepFornece.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCnpjFornece_KeyPress);
            // 
            // txtemailFornece
            // 
            this.txtemailFornece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtemailFornece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemailFornece.Location = new System.Drawing.Point(135, 254);
            this.txtemailFornece.MaxLength = 20;
            this.txtemailFornece.Name = "txtemailFornece";
            this.txtemailFornece.Size = new System.Drawing.Size(190, 20);
            this.txtemailFornece.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(78, 254);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 21);
            this.label10.TabIndex = 29;
            this.label10.Text = "Email";
            // 
            // txtnumeroFornece
            // 
            this.txtnumeroFornece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnumeroFornece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnumeroFornece.Location = new System.Drawing.Point(135, 219);
            this.txtnumeroFornece.MaxLength = 5;
            this.txtnumeroFornece.Name = "txtnumeroFornece";
            this.txtnumeroFornece.Size = new System.Drawing.Size(190, 20);
            this.txtnumeroFornece.TabIndex = 6;
            this.txtnumeroFornece.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtbCnpjFornece_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(53, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 21);
            this.label9.TabIndex = 27;
            this.label9.Text = "Número ";
            // 
            // txtComplementoFornece
            // 
            this.txtComplementoFornece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComplementoFornece.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplementoFornece.Location = new System.Drawing.Point(135, 178);
            this.txtComplementoFornece.MaxLength = 20;
            this.txtComplementoFornece.Name = "txtComplementoFornece";
            this.txtComplementoFornece.Size = new System.Drawing.Size(190, 20);
            this.txtComplementoFornece.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 21);
            this.label8.TabIndex = 25;
            this.label8.Text = "Complemento";
            // 
            // txtNomeFornec
            // 
            this.txtNomeFornec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeFornec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeFornec.Location = new System.Drawing.Point(135, 22);
            this.txtNomeFornec.MaxLength = 20;
            this.txtNomeFornec.Name = "txtNomeFornec";
            this.txtNomeFornec.Size = new System.Drawing.Size(190, 20);
            this.txtNomeFornec.TabIndex = 1;
            this.txtNomeFornec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeFornec_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(53, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 21);
            this.label4.TabIndex = 20;
            this.label4.Text = "Telefone";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(72, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 21);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nome";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(82, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 21);
            this.label6.TabIndex = 17;
            this.label6.Text = "Cnpj";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(85, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "Cep";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(926, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // BtnSalvarFornecedor
            // 
            this.BtnSalvarFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalvarFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSalvarFornecedor.FlatAppearance.BorderSize = 0;
            this.BtnSalvarFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSalvarFornecedor.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalvarFornecedor.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalvarFornecedor.Image")));
            this.BtnSalvarFornecedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSalvarFornecedor.Location = new System.Drawing.Point(870, 483);
            this.BtnSalvarFornecedor.Name = "BtnSalvarFornecedor";
            this.BtnSalvarFornecedor.Size = new System.Drawing.Size(122, 39);
            this.BtnSalvarFornecedor.TabIndex = 2;
            this.BtnSalvarFornecedor.Text = "Salvar";
            this.BtnSalvarFornecedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSalvarFornecedor.UseVisualStyleBackColor = false;
            this.BtnSalvarFornecedor.Click += new System.EventHandler(this.BtnSalvarFornecedor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 36);
            this.label2.TabIndex = 4;
            this.label2.Text = "Fornecedor";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Snow;
            this.tabPage1.Controls.Add(this.btnalterar);
            this.tabPage1.Controls.Add(this.btnApagar);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.dgvConsultarFornecedor);
            this.tabPage1.Controls.Add(this.txtConsultarFornecedor);
            this.tabPage1.Controls.Add(this.btnconsultarFornecedor);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(998, 528);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Consultar/Apagar/Alterar";
            // 
            // btnalterar
            // 
            this.btnalterar.BackColor = System.Drawing.Color.Transparent;
            this.btnalterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnalterar.FlatAppearance.BorderSize = 0;
            this.btnalterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnalterar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnalterar.Image = ((System.Drawing.Image)(resources.GetObject("btnalterar.Image")));
            this.btnalterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnalterar.Location = new System.Drawing.Point(751, 37);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(119, 39);
            this.btnalterar.TabIndex = 10;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnalterar.UseVisualStyleBackColor = false;
            this.btnalterar.Click += new System.EventHandler(this.btnalterar_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.BackColor = System.Drawing.Color.Transparent;
            this.btnApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApagar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApagar.Location = new System.Drawing.Point(630, 36);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(115, 39);
            this.btnApagar.TabIndex = 9;
            this.btnApagar.Text = "Apagar";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(929, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // dgvConsultarFornecedor
            // 
            this.dgvConsultarFornecedor.AllowUserToAddRows = false;
            this.dgvConsultarFornecedor.AllowUserToDeleteRows = false;
            this.dgvConsultarFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this.dgvConsultarFornecedor.Location = new System.Drawing.Point(9, 82);
            this.dgvConsultarFornecedor.Name = "dgvConsultarFornecedor";
            this.dgvConsultarFornecedor.ReadOnly = true;
            this.dgvConsultarFornecedor.RowHeadersVisible = false;
            this.dgvConsultarFornecedor.Size = new System.Drawing.Size(973, 434);
            this.dgvConsultarFornecedor.TabIndex = 20;
            this.dgvConsultarFornecedor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConsultarFornecedor_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "nm_nome";
            this.Column1.HeaderText = "Nome";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "ds_cnpj";
            this.Column2.HeaderText = "Cnpj";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "ds_telefone";
            this.Column3.HeaderText = "Telefone";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.DataPropertyName = "ds_cep";
            this.Column4.HeaderText = "CEP";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.DataPropertyName = "ds_complemento";
            this.Column5.HeaderText = "Complemento";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column6.DataPropertyName = "ds_numerofornecedor";
            this.Column6.HeaderText = "Número";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column7.DataPropertyName = "ds_email";
            this.Column7.HeaderText = "Email";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // txtConsultarFornecedor
            // 
            this.txtConsultarFornecedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConsultarFornecedor.Location = new System.Drawing.Point(9, 53);
            this.txtConsultarFornecedor.MaxLength = 50;
            this.txtConsultarFornecedor.Name = "txtConsultarFornecedor";
            this.txtConsultarFornecedor.Size = new System.Drawing.Size(479, 23);
            this.txtConsultarFornecedor.TabIndex = 7;
            // 
            // btnconsultarFornecedor
            // 
            this.btnconsultarFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.btnconsultarFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnconsultarFornecedor.FlatAppearance.BorderSize = 0;
            this.btnconsultarFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnconsultarFornecedor.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconsultarFornecedor.Image = ((System.Drawing.Image)(resources.GetObject("btnconsultarFornecedor.Image")));
            this.btnconsultarFornecedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnconsultarFornecedor.Location = new System.Drawing.Point(493, 37);
            this.btnconsultarFornecedor.Name = "btnconsultarFornecedor";
            this.btnconsultarFornecedor.Size = new System.Drawing.Size(131, 39);
            this.btnconsultarFornecedor.TabIndex = 8;
            this.btnconsultarFornecedor.Text = "Consultar";
            this.btnconsultarFornecedor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnconsultarFornecedor.UseVisualStyleBackColor = false;
            this.btnconsultarFornecedor.Click += new System.EventHandler(this.btnconsultarFornecedor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 36);
            this.label1.TabIndex = 5;
            this.label1.Text = "Fornecedor";
            // 
            // Fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(1030, 592);
            this.Controls.Add(this.tcCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Fornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fornecedor";
            this.tcCadastrarFuncionario.ResumeLayout(false);
            this.Cadastrar.ResumeLayout(false);
            this.Cadastrar.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFornecedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcCadastrarFuncionario;
        private System.Windows.Forms.TabPage Cadastrar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnSalvarFornecedor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtemailFornece;
        private System.Windows.Forms.TextBox txtnumeroFornece;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtComplementoFornece;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNomeFornec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mtbCepFornece;
        private System.Windows.Forms.MaskedTextBox mtbTelefoneFornecedor;
        private System.Windows.Forms.MaskedTextBox mtbCnpjFornece;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvConsultarFornecedor;
        private System.Windows.Forms.TextBox txtConsultarFornecedor;
        private System.Windows.Forms.Button btnconsultarFornecedor;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
    }
}