﻿using Michaelpops.Programação;
using Michaelpops.Programação.Fornecedor;
using Michaelpops.Telas.CRUD.Fornec;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class Fornecedor : Form
    {
        Validação v = new Validação();
        public Fornecedor()
        {
            InitializeComponent();
        }


        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void txtNomeFornec_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void mtbCnpjFornece_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarFornecedor.CurrentRow != null)
                {
                    FornecedorDTO fornecedor = dgvConsultarFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;

                    AlterarFornecedor tela = new AlterarFornecedor();
                    tela.LoadScreen(fornecedor);
                    tela.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Selecione um Fornecedor");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
           
        }

        private void BtnSalvarFornecedor_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.nm_nome = txtNomeFornec.Text;
                dto.ds_cnpj = mtbCnpjFornece.Text;
                dto.ds_telefone = mtbTelefoneFornecedor.Text;
                dto.ds_cep = mtbCepFornece.Text;
                dto.ds_complemento = txtComplementoFornece.Text;
                dto.ds_numerofornecedor = txtnumeroFornece.Text;
                dto.ds_email = txtemailFornece.Text;

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);
                MessageBox.Show("Fornecedor salvo com sucesso.");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro " + ex.Message);
            }
        }

        private void btnconsultarFornecedor_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> a = business.Consultar(txtConsultarFornecedor.Text);
            dgvConsultarFornecedor.AutoGenerateColumns = false;
            dgvConsultarFornecedor.DataSource = a;
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarFornecedor.CurrentRow != null)
                {
                    FornecedorDTO fornecedor = dgvConsultarFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse fornecedor?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        FornecedorBusiness business = new FornecedorBusiness();
                        business.Remover(fornecedor.id_fornecedor);

                        List<FornecedorDTO> a = business.Consultar(txtConsultarFornecedor.Text);
                        dgvConsultarFornecedor.AutoGenerateColumns = false;
                        dgvConsultarFornecedor.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um cliente");
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void dgvConsultarFornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
