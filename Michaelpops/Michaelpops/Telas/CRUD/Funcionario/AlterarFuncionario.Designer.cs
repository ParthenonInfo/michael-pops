﻿namespace Michaelpops.Telas.CRUD
{
    partial class AlterarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarFuncionario));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsalariobruto = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txttelmovel = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtncasa = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtsobrenome = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbodepartamento = new System.Windows.Forms.ComboBox();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.txtrg = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dtpnascimento = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.txtcarteiratrabalho = new System.Windows.Forms.MaskedTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtlogin = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.rbnvendedor = new System.Windows.Forms.RadioButton();
            this.rbnadm = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(958, 540);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Snow;
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(950, 510);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Alterar Funcionário";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(878, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 42;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(825, 457);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 39);
            this.button1.TabIndex = 17;
            this.button1.Text = "Alterar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbnvendedor);
            this.groupBox3.Controls.Add(this.rbnadm);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtsalariobruto);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.txtcep);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.txttelmovel);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.txtcomplemento);
            this.groupBox3.Controls.Add(this.txttelefone);
            this.groupBox3.Controls.Add(this.txtncasa);
            this.groupBox3.Location = new System.Drawing.Point(396, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(395, 386);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 21);
            this.label1.TabIndex = 20;
            this.label1.Text = "Salário Bruto";
            // 
            // txtsalariobruto
            // 
            this.txtsalariobruto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsalariobruto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsalariobruto.Location = new System.Drawing.Point(150, 255);
            this.txtsalariobruto.MaxLength = 5;
            this.txtsalariobruto.Name = "txtsalariobruto";
            this.txtsalariobruto.Size = new System.Drawing.Size(190, 21);
            this.txtsalariobruto.TabIndex = 16;
            this.txtsalariobruto.TextChanged += new System.EventHandler(this.txtsalariobruto_TextChanged);
            this.txtsalariobruto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsalariobruto_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(68, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 21);
            this.label18.TabIndex = 12;
            this.label18.Text = "Telefone";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 70);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(128, 21);
            this.label19.TabIndex = 13;
            this.label19.Text = "Telefone Móvel";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(100, 113);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 21);
            this.label20.TabIndex = 14;
            this.label20.Text = "Cep";
            // 
            // txtcep
            // 
            this.txtcep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcep.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcep.Location = new System.Drawing.Point(150, 113);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(190, 21);
            this.txtcep.TabIndex = 13;
            this.txtcep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(20, 155);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 42);
            this.label21.TabIndex = 15;
            this.label21.Text = "Número \r\nComplemento";
            // 
            // txttelmovel
            // 
            this.txttelmovel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttelmovel.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelmovel.Location = new System.Drawing.Point(150, 69);
            this.txttelmovel.Mask = "(00) 00000-0000";
            this.txttelmovel.Name = "txttelmovel";
            this.txttelmovel.Size = new System.Drawing.Size(190, 21);
            this.txttelmovel.TabIndex = 12;
            this.txttelmovel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1, 211);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(143, 21);
            this.label22.TabIndex = 16;
            this.label22.Text = "Número da Casa";
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcomplemento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcomplemento.Location = new System.Drawing.Point(150, 158);
            this.txtcomplemento.MaxLength = 10;
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(190, 21);
            this.txtcomplemento.TabIndex = 14;
            this.txtcomplemento.Text = "(Opcional)";
            // 
            // txttelefone
            // 
            this.txttelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttelefone.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefone.Location = new System.Drawing.Point(150, 24);
            this.txttelefone.Mask = "(00) 0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(190, 21);
            this.txttelefone.TabIndex = 11;
            this.txttelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // txtncasa
            // 
            this.txtncasa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtncasa.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtncasa.Location = new System.Drawing.Point(150, 210);
            this.txtncasa.MaxLength = 5;
            this.txtncasa.Name = "txtncasa";
            this.txtncasa.Size = new System.Drawing.Size(190, 21);
            this.txtncasa.TabIndex = 15;
            this.txtncasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.txtsobrenome);
            this.groupBox4.Controls.Add(this.txtnome);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.cbodepartamento);
            this.groupBox4.Controls.Add(this.txtcpf);
            this.groupBox4.Controls.Add(this.txtrg);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.dtpnascimento);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.txtcarteiratrabalho);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.txtemail);
            this.groupBox4.Controls.Add(this.txtlogin);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.txtsenha);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Location = new System.Drawing.Point(6, 42);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(384, 454);
            this.groupBox4.TabIndex = 39;
            this.groupBox4.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(74, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 21);
            this.label23.TabIndex = 5;
            this.label23.Text = "Nome";
            // 
            // txtsobrenome
            // 
            this.txtsobrenome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsobrenome.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsobrenome.Location = new System.Drawing.Point(138, 63);
            this.txtsobrenome.MaxLength = 20;
            this.txtsobrenome.Name = "txtsobrenome";
            this.txtsobrenome.Size = new System.Drawing.Size(190, 21);
            this.txtsobrenome.TabIndex = 2;
            this.txtsobrenome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // txtnome
            // 
            this.txtnome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnome.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome.Location = new System.Drawing.Point(138, 20);
            this.txtnome.MaxLength = 20;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(190, 21);
            this.txtnome.TabIndex = 1;
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(27, 186);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(104, 42);
            this.label24.TabIndex = 17;
            this.label24.Text = "Data de \r\nNascimento";
            // 
            // cbodepartamento
            // 
            this.cbodepartamento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbodepartamento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbodepartamento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbodepartamento.FormattingEnabled = true;
            this.cbodepartamento.Location = new System.Drawing.Point(138, 362);
            this.cbodepartamento.MaxLength = 1;
            this.cbodepartamento.Name = "cbodepartamento";
            this.cbodepartamento.Size = new System.Drawing.Size(190, 24);
            this.cbodepartamento.TabIndex = 9;
            this.cbodepartamento.SelectedIndexChanged += new System.EventHandler(this.cbodepartamento_SelectedIndexChanged);
            // 
            // txtcpf
            // 
            this.txtcpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcpf.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcpf.Location = new System.Drawing.Point(138, 108);
            this.txtcpf.Mask = "000.000.000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(190, 21);
            this.txtcpf.TabIndex = 3;
            this.txtcpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // txtrg
            // 
            this.txtrg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrg.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrg.Location = new System.Drawing.Point(138, 151);
            this.txtrg.Mask = "00.000.000-0";
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(190, 21);
            this.txtrg.TabIndex = 4;
            this.txtrg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(100, 147);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 21);
            this.label25.TabIndex = 9;
            this.label25.Text = "Rg";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(92, 107);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(39, 21);
            this.label26.TabIndex = 8;
            this.label26.Text = "Cpf";
            // 
            // dtpnascimento
            // 
            this.dtpnascimento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpnascimento.Location = new System.Drawing.Point(138, 187);
            this.dtpnascimento.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpnascimento.MinDate = new System.DateTime(1910, 1, 1, 0, 0, 0, 0);
            this.dtpnascimento.Name = "dtpnascimento";
            this.dtpnascimento.Size = new System.Drawing.Size(190, 21);
            this.dtpnascimento.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(32, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(99, 21);
            this.label27.TabIndex = 6;
            this.label27.Text = "Sobrenome";
            // 
            // txtcarteiratrabalho
            // 
            this.txtcarteiratrabalho.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcarteiratrabalho.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcarteiratrabalho.Location = new System.Drawing.Point(138, 401);
            this.txtcarteiratrabalho.Mask = "000.00 000.00-0";
            this.txtcarteiratrabalho.Name = "txtcarteiratrabalho";
            this.txtcarteiratrabalho.Size = new System.Drawing.Size(190, 21);
            this.txtcarteiratrabalho.TabIndex = 10;
            this.txtcarteiratrabalho.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox4_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(80, 240);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 21);
            this.label28.TabIndex = 11;
            this.label28.Text = "Email";
            // 
            // txtemail
            // 
            this.txtemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtemail.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.Location = new System.Drawing.Point(138, 241);
            this.txtemail.MaxLength = 40;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(190, 21);
            this.txtemail.TabIndex = 6;
            // 
            // txtlogin
            // 
            this.txtlogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlogin.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlogin.Location = new System.Drawing.Point(138, 284);
            this.txtlogin.MaxLength = 20;
            this.txtlogin.Name = "txtlogin";
            this.txtlogin.Size = new System.Drawing.Size(190, 21);
            this.txtlogin.TabIndex = 7;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(27, 397);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 42);
            this.label29.TabIndex = 7;
            this.label29.Text = "Carteira de \r\nTrabalho\r\n";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(72, 319);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(59, 21);
            this.label30.TabIndex = 19;
            this.label30.Text = "Senha";
            // 
            // txtsenha
            // 
            this.txtsenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsenha.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.Location = new System.Drawing.Point(138, 323);
            this.txtsenha.MaxLength = 20;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(190, 21);
            this.txtsenha.TabIndex = 8;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(80, 280);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 21);
            this.label31.TabIndex = 18;
            this.label31.Text = "Login";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(2, 361);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(129, 21);
            this.label32.TabIndex = 10;
            this.label32.Text = "Departamento";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(183, 36);
            this.label33.TabIndex = 38;
            this.label33.Text = "Funcionário";
            // 
            // rbnvendedor
            // 
            this.rbnvendedor.AutoSize = true;
            this.rbnvendedor.Location = new System.Drawing.Point(150, 346);
            this.rbnvendedor.Name = "rbnvendedor";
            this.rbnvendedor.Size = new System.Drawing.Size(166, 21);
            this.rbnvendedor.TabIndex = 24;
            this.rbnvendedor.TabStop = true;
            this.rbnvendedor.Text = "Permissão de vendedor";
            this.rbnvendedor.UseVisualStyleBackColor = true;
            // 
            // rbnadm
            // 
            this.rbnadm.AutoSize = true;
            this.rbnadm.Location = new System.Drawing.Point(150, 293);
            this.rbnadm.Name = "rbnadm";
            this.rbnadm.Size = new System.Drawing.Size(148, 21);
            this.rbnadm.TabIndex = 23;
            this.rbnadm.TabStop = true;
            this.rbnadm.Text = "Permissão para ADM";
            this.rbnadm.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 21);
            this.label2.TabIndex = 22;
            this.label2.Text = "Permissões: ";
            // 
            // AlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(982, 564);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlterarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AlterarFuncionario";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtsobrenome;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbodepartamento;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.MaskedTextBox txtrg;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker dtpnascimento;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox txtcarteiratrabalho;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtlogin;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox txttelmovel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.TextBox txtncasa;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsalariobruto;
        private System.Windows.Forms.RadioButton rbnvendedor;
        private System.Windows.Forms.RadioButton rbnadm;
        private System.Windows.Forms.Label label2;
    }
}