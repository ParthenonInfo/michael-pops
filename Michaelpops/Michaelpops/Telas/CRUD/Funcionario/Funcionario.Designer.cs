﻿namespace Michaelpops.Telas
{
    partial class Funcionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Funcionarios));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnalterar = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.dgvConsultarFuncionarios = new System.Windows.Forms.DataGridView();
            this.txtConsultarFuncionario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnConsultarFuncionario = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Cadastrar = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdnvendedor = new System.Windows.Forms.RadioButton();
            this.rbnADM = new System.Windows.Forms.RadioButton();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtsalariobruto = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.mtbCepFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.mtbTelefonemóvelFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNcomplementoFuncionario = new System.Windows.Forms.TextBox();
            this.mtbTelefoneFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.txtNcasaFuncionario = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSobrenomeFuncionario = new System.Windows.Forms.TextBox();
            this.TxtNomeFuncionario = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbDepartamentoFuncionario = new System.Windows.Forms.ComboBox();
            this.mtbCpfFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.mtbRgFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpNascimentoFuncionario = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.mtbCarteiraFuncionario = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEmailFuncionario = new System.Windows.Forms.TextBox();
            this.txtLoginFuncionario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSenhaFuncionario = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSalvarFuncionario = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tcCadastrarFuncionario = new System.Windows.Forms.TabControl();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Departamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sobrenome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFuncionarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.Cadastrar.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tcCadastrarFuncionario.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Snow;
            this.tabPage2.Controls.Add(this.btnalterar);
            this.tabPage2.Controls.Add(this.btnApagar);
            this.tabPage2.Controls.Add(this.dgvConsultarFuncionarios);
            this.tabPage2.Controls.Add(this.txtConsultarFuncionario);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.BtnConsultarFuncionario);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(998, 528);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consultar/Apagar/Alterar";
            // 
            // btnalterar
            // 
            this.btnalterar.BackColor = System.Drawing.Color.Transparent;
            this.btnalterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnalterar.FlatAppearance.BorderSize = 0;
            this.btnalterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnalterar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnalterar.Image = ((System.Drawing.Image)(resources.GetObject("btnalterar.Image")));
            this.btnalterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnalterar.Location = new System.Drawing.Point(762, 43);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(119, 39);
            this.btnalterar.TabIndex = 19;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnalterar.UseVisualStyleBackColor = false;
            this.btnalterar.Click += new System.EventHandler(this.btnalterar_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.BackColor = System.Drawing.Color.Transparent;
            this.btnApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApagar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApagar.Location = new System.Drawing.Point(641, 42);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(115, 39);
            this.btnApagar.TabIndex = 18;
            this.btnApagar.Text = "Apagar";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // dgvConsultarFuncionarios
            // 
            this.dgvConsultarFuncionarios.AllowUserToAddRows = false;
            this.dgvConsultarFuncionarios.AllowUserToDeleteRows = false;
            this.dgvConsultarFuncionarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarFuncionarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Telefone,
            this.Email,
            this.Departamento,
            this.Sobrenome,
            this.Nome});
            this.dgvConsultarFuncionarios.Location = new System.Drawing.Point(21, 88);
            this.dgvConsultarFuncionarios.Name = "dgvConsultarFuncionarios";
            this.dgvConsultarFuncionarios.ReadOnly = true;
            this.dgvConsultarFuncionarios.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvConsultarFuncionarios.RowHeadersVisible = false;
            this.dgvConsultarFuncionarios.Size = new System.Drawing.Size(960, 434);
            this.dgvConsultarFuncionarios.TabIndex = 17;
            // 
            // txtConsultarFuncionario
            // 
            this.txtConsultarFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConsultarFuncionario.Location = new System.Drawing.Point(21, 59);
            this.txtConsultarFuncionario.MaxLength = 50;
            this.txtConsultarFuncionario.Name = "txtConsultarFuncionario";
            this.txtConsultarFuncionario.Size = new System.Drawing.Size(479, 23);
            this.txtConsultarFuncionario.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "Funcionário";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // BtnConsultarFuncionario
            // 
            this.BtnConsultarFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.BtnConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnConsultarFuncionario.FlatAppearance.BorderSize = 0;
            this.BtnConsultarFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnConsultarFuncionario.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConsultarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultarFuncionario.Image")));
            this.BtnConsultarFuncionario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnConsultarFuncionario.Location = new System.Drawing.Point(504, 43);
            this.BtnConsultarFuncionario.Name = "BtnConsultarFuncionario";
            this.BtnConsultarFuncionario.Size = new System.Drawing.Size(131, 39);
            this.BtnConsultarFuncionario.TabIndex = 5;
            this.BtnConsultarFuncionario.Text = "Consultar";
            this.BtnConsultarFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnConsultarFuncionario.UseVisualStyleBackColor = false;
            this.BtnConsultarFuncionario.Click += new System.EventHandler(this.BtnConsultarFuncionario_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(926, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // Cadastrar
            // 
            this.Cadastrar.BackColor = System.Drawing.Color.Snow;
            this.Cadastrar.Controls.Add(this.groupBox2);
            this.Cadastrar.Controls.Add(this.groupBox1);
            this.Cadastrar.Controls.Add(this.pictureBox1);
            this.Cadastrar.Controls.Add(this.BtnSalvarFuncionario);
            this.Cadastrar.Controls.Add(this.label2);
            this.Cadastrar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Cadastrar.Location = new System.Drawing.Point(4, 26);
            this.Cadastrar.Name = "Cadastrar";
            this.Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.Cadastrar.Size = new System.Drawing.Size(998, 528);
            this.Cadastrar.TabIndex = 0;
            this.Cadastrar.Text = "Cadastrar";
            this.Cadastrar.Click += new System.EventHandler(this.Cadastrar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdnvendedor);
            this.groupBox2.Controls.Add(this.rbnADM);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtsalariobruto);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.mtbCepFuncionario);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.mtbTelefonemóvelFuncionario);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtNcomplementoFuncionario);
            this.groupBox2.Controls.Add(this.mtbTelefoneFuncionario);
            this.groupBox2.Controls.Add(this.txtNcasaFuncionario);
            this.groupBox2.Location = new System.Drawing.Point(385, 45);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(395, 388);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            // 
            // rdnvendedor
            // 
            this.rdnvendedor.AutoSize = true;
            this.rdnvendedor.Location = new System.Drawing.Point(165, 339);
            this.rdnvendedor.Name = "rdnvendedor";
            this.rdnvendedor.Size = new System.Drawing.Size(178, 21);
            this.rdnvendedor.TabIndex = 21;
            this.rdnvendedor.TabStop = true;
            this.rdnvendedor.Text = "Permissão de vendedor";
            this.rdnvendedor.UseVisualStyleBackColor = true;
            // 
            // rbnADM
            // 
            this.rbnADM.AutoSize = true;
            this.rbnADM.Location = new System.Drawing.Point(165, 286);
            this.rbnADM.Name = "rbnADM";
            this.rbnADM.Size = new System.Drawing.Size(159, 21);
            this.rbnADM.TabIndex = 20;
            this.rbnADM.TabStop = true;
            this.rbnADM.Text = "Permissão para ADM";
            this.rbnADM.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(61, 312);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 21);
            this.label19.TabIndex = 19;
            this.label19.Text = "Permissões: ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(53, 245);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 21);
            this.label18.TabIndex = 18;
            this.label18.Text = "Salário Bruto";
            // 
            // txtsalariobruto
            // 
            this.txtsalariobruto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsalariobruto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsalariobruto.Location = new System.Drawing.Point(165, 243);
            this.txtsalariobruto.MaxLength = 7;
            this.txtsalariobruto.Name = "txtsalariobruto";
            this.txtsalariobruto.Size = new System.Drawing.Size(190, 21);
            this.txtsalariobruto.TabIndex = 16;
            this.txtsalariobruto.TextChanged += new System.EventHandler(this.txtsalariobruto_TextChanged);
            this.txtsalariobruto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsalariobruto_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(83, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 21);
            this.label10.TabIndex = 12;
            this.label10.Text = "Telefone";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(31, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 21);
            this.label11.TabIndex = 13;
            this.label11.Text = "Telefone Móvel";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(115, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 21);
            this.label12.TabIndex = 14;
            this.label12.Text = "Cep";
            // 
            // mtbCepFuncionario
            // 
            this.mtbCepFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCepFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCepFuncionario.Location = new System.Drawing.Point(165, 114);
            this.mtbCepFuncionario.Mask = "00000-000";
            this.mtbCepFuncionario.Name = "mtbCepFuncionario";
            this.mtbCepFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbCepFuncionario.TabIndex = 13;
            this.mtbCepFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(35, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(124, 42);
            this.label13.TabIndex = 15;
            this.label13.Text = "Número \r\nComplemento";
            // 
            // mtbTelefonemóvelFuncionario
            // 
            this.mtbTelefonemóvelFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbTelefonemóvelFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbTelefonemóvelFuncionario.Location = new System.Drawing.Point(165, 70);
            this.mtbTelefonemóvelFuncionario.Mask = "(00) 00000-0000";
            this.mtbTelefonemóvelFuncionario.Name = "mtbTelefonemóvelFuncionario";
            this.mtbTelefonemóvelFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbTelefonemóvelFuncionario.TabIndex = 12;
            this.mtbTelefonemóvelFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 203);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 21);
            this.label14.TabIndex = 16;
            this.label14.Text = "Número da Casa";
            // 
            // txtNcomplementoFuncionario
            // 
            this.txtNcomplementoFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNcomplementoFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNcomplementoFuncionario.Location = new System.Drawing.Point(165, 159);
            this.txtNcomplementoFuncionario.MaxLength = 10;
            this.txtNcomplementoFuncionario.Name = "txtNcomplementoFuncionario";
            this.txtNcomplementoFuncionario.Size = new System.Drawing.Size(190, 21);
            this.txtNcomplementoFuncionario.TabIndex = 14;
            this.txtNcomplementoFuncionario.Text = "(Opcional)";
            this.txtNcomplementoFuncionario.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtNcomplementoFuncionario_MouseClick);
            this.txtNcomplementoFuncionario.TextChanged += new System.EventHandler(this.txtNcomplementoFuncionario_TextChanged);
            this.txtNcomplementoFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // mtbTelefoneFuncionario
            // 
            this.mtbTelefoneFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbTelefoneFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbTelefoneFuncionario.Location = new System.Drawing.Point(165, 25);
            this.mtbTelefoneFuncionario.Mask = "(00) 0000-0000";
            this.mtbTelefoneFuncionario.Name = "mtbTelefoneFuncionario";
            this.mtbTelefoneFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbTelefoneFuncionario.TabIndex = 11;
            this.mtbTelefoneFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // txtNcasaFuncionario
            // 
            this.txtNcasaFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNcasaFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNcasaFuncionario.Location = new System.Drawing.Point(165, 201);
            this.txtNcasaFuncionario.MaxLength = 5;
            this.txtNcasaFuncionario.Name = "txtNcasaFuncionario";
            this.txtNcasaFuncionario.Size = new System.Drawing.Size(190, 21);
            this.txtNcasaFuncionario.TabIndex = 15;
            this.txtNcasaFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtSobrenomeFuncionario);
            this.groupBox1.Controls.Add(this.TxtNomeFuncionario);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cbDepartamentoFuncionario);
            this.groupBox1.Controls.Add(this.mtbCpfFuncionario);
            this.groupBox1.Controls.Add(this.mtbRgFuncionario);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpNascimentoFuncionario);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.mtbCarteiraFuncionario);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtEmailFuncionario);
            this.groupBox1.Controls.Add(this.txtLoginFuncionario);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtSenhaFuncionario);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(6, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(373, 462);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(70, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nome";
            // 
            // TxtSobrenomeFuncionario
            // 
            this.TxtSobrenomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSobrenomeFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSobrenomeFuncionario.Location = new System.Drawing.Point(133, 65);
            this.TxtSobrenomeFuncionario.MaxLength = 20;
            this.TxtSobrenomeFuncionario.Name = "TxtSobrenomeFuncionario";
            this.TxtSobrenomeFuncionario.Size = new System.Drawing.Size(190, 21);
            this.TxtSobrenomeFuncionario.TabIndex = 2;
            this.TxtSobrenomeFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeFuncionario_KeyPress);
            // 
            // TxtNomeFuncionario
            // 
            this.TxtNomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNomeFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNomeFuncionario.Location = new System.Drawing.Point(133, 22);
            this.TxtNomeFuncionario.MaxLength = 20;
            this.TxtNomeFuncionario.Name = "TxtNomeFuncionario";
            this.TxtNomeFuncionario.Size = new System.Drawing.Size(190, 21);
            this.TxtNomeFuncionario.TabIndex = 1;
            this.TxtNomeFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeFuncionario_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(23, 188);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 42);
            this.label15.TabIndex = 17;
            this.label15.Text = "Data de \r\nNascimento";
            // 
            // cbDepartamentoFuncionario
            // 
            this.cbDepartamentoFuncionario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbDepartamentoFuncionario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbDepartamentoFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDepartamentoFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDepartamentoFuncionario.FormattingEnabled = true;
            this.cbDepartamentoFuncionario.Location = new System.Drawing.Point(133, 364);
            this.cbDepartamentoFuncionario.MaxLength = 30;
            this.cbDepartamentoFuncionario.Name = "cbDepartamentoFuncionario";
            this.cbDepartamentoFuncionario.Size = new System.Drawing.Size(190, 24);
            this.cbDepartamentoFuncionario.TabIndex = 9;
            this.cbDepartamentoFuncionario.SelectedIndexChanged += new System.EventHandler(this.cbDepartamentoFuncionario_SelectedIndexChanged);
            this.cbDepartamentoFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeFuncionario_KeyPress);
            // 
            // mtbCpfFuncionario
            // 
            this.mtbCpfFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCpfFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCpfFuncionario.Location = new System.Drawing.Point(133, 110);
            this.mtbCpfFuncionario.Mask = "000.000.000-00";
            this.mtbCpfFuncionario.Name = "mtbCpfFuncionario";
            this.mtbCpfFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbCpfFuncionario.TabIndex = 3;
            this.mtbCpfFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // mtbRgFuncionario
            // 
            this.mtbRgFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbRgFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbRgFuncionario.Location = new System.Drawing.Point(133, 153);
            this.mtbRgFuncionario.Mask = "00.000.000-0";
            this.mtbRgFuncionario.Name = "mtbRgFuncionario";
            this.mtbRgFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbRgFuncionario.TabIndex = 4;
            this.mtbRgFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(96, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 21);
            this.label7.TabIndex = 9;
            this.label7.Text = "Rg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(88, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 21);
            this.label6.TabIndex = 8;
            this.label6.Text = "Cpf";
            // 
            // dtpNascimentoFuncionario
            // 
            this.dtpNascimentoFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimentoFuncionario.Location = new System.Drawing.Point(133, 199);
            this.dtpNascimentoFuncionario.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpNascimentoFuncionario.MinDate = new System.DateTime(1910, 1, 1, 0, 0, 0, 0);
            this.dtpNascimentoFuncionario.Name = "dtpNascimentoFuncionario";
            this.dtpNascimentoFuncionario.Size = new System.Drawing.Size(190, 21);
            this.dtpNascimentoFuncionario.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(28, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sobrenome";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // mtbCarteiraFuncionario
            // 
            this.mtbCarteiraFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtbCarteiraFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbCarteiraFuncionario.Location = new System.Drawing.Point(133, 404);
            this.mtbCarteiraFuncionario.Mask = "000.00 000.00-0";
            this.mtbCarteiraFuncionario.Name = "mtbCarteiraFuncionario";
            this.mtbCarteiraFuncionario.Size = new System.Drawing.Size(190, 21);
            this.mtbCarteiraFuncionario.TabIndex = 10;
            this.mtbCarteiraFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNcomplementoFuncionario_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(76, 242);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 21);
            this.label9.TabIndex = 11;
            this.label9.Text = "Email";
            // 
            // txtEmailFuncionario
            // 
            this.txtEmailFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailFuncionario.Location = new System.Drawing.Point(133, 243);
            this.txtEmailFuncionario.MaxLength = 40;
            this.txtEmailFuncionario.Name = "txtEmailFuncionario";
            this.txtEmailFuncionario.Size = new System.Drawing.Size(190, 21);
            this.txtEmailFuncionario.TabIndex = 6;
            this.txtEmailFuncionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmailFuncionario_KeyPress);
            // 
            // txtLoginFuncionario
            // 
            this.txtLoginFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoginFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoginFuncionario.Location = new System.Drawing.Point(133, 286);
            this.txtLoginFuncionario.MaxLength = 20;
            this.txtLoginFuncionario.Name = "txtLoginFuncionario";
            this.txtLoginFuncionario.Size = new System.Drawing.Size(190, 21);
            this.txtLoginFuncionario.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 398);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 42);
            this.label5.TabIndex = 7;
            this.label5.Text = "Carteira de \r\nTrabalho";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(68, 321);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 21);
            this.label17.TabIndex = 19;
            this.label17.Text = "Senha";
            // 
            // txtSenhaFuncionario
            // 
            this.txtSenhaFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenhaFuncionario.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenhaFuncionario.Location = new System.Drawing.Point(133, 325);
            this.txtSenhaFuncionario.MaxLength = 20;
            this.txtSenhaFuncionario.Name = "txtSenhaFuncionario";
            this.txtSenhaFuncionario.Size = new System.Drawing.Size(190, 21);
            this.txtSenhaFuncionario.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(76, 282);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 21);
            this.label16.TabIndex = 18;
            this.label16.Text = "Login";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(-2, 363);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 21);
            this.label8.TabIndex = 10;
            this.label8.Text = "Departamento";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(926, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // BtnSalvarFuncionario
            // 
            this.BtnSalvarFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSalvarFuncionario.FlatAppearance.BorderSize = 0;
            this.BtnSalvarFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSalvarFuncionario.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalvarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalvarFuncionario.Image")));
            this.BtnSalvarFuncionario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSalvarFuncionario.Location = new System.Drawing.Point(870, 483);
            this.BtnSalvarFuncionario.Name = "BtnSalvarFuncionario";
            this.BtnSalvarFuncionario.Size = new System.Drawing.Size(122, 39);
            this.BtnSalvarFuncionario.TabIndex = 2;
            this.BtnSalvarFuncionario.Text = "Salvar";
            this.BtnSalvarFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSalvarFuncionario.UseVisualStyleBackColor = false;
            this.BtnSalvarFuncionario.Click += new System.EventHandler(this.BtnSalvarFuncionario_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 36);
            this.label2.TabIndex = 4;
            this.label2.Text = "Funcionário";
            // 
            // tcCadastrarFuncionario
            // 
            this.tcCadastrarFuncionario.Controls.Add(this.Cadastrar);
            this.tcCadastrarFuncionario.Controls.Add(this.tabPage2);
            this.tcCadastrarFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCadastrarFuncionario.Location = new System.Drawing.Point(12, 12);
            this.tcCadastrarFuncionario.Name = "tcCadastrarFuncionario";
            this.tcCadastrarFuncionario.SelectedIndex = 0;
            this.tcCadastrarFuncionario.Size = new System.Drawing.Size(1006, 558);
            this.tcCadastrarFuncionario.TabIndex = 35;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "vl_salariobruto";
            this.Column1.HeaderText = "Salario Bruto";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Telefone
            // 
            this.Telefone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Telefone.DataPropertyName = "ds_telefonemovel";
            this.Telefone.HeaderText = "Telefone Móvel";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Email.DataPropertyName = "ds_email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // Departamento
            // 
            this.Departamento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Departamento.DataPropertyName = "ds_dpto";
            this.Departamento.HeaderText = "Departamento";
            this.Departamento.Name = "Departamento";
            this.Departamento.ReadOnly = true;
            // 
            // Sobrenome
            // 
            this.Sobrenome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Sobrenome.DataPropertyName = "nm_sobrenome";
            this.Sobrenome.HeaderText = "Sobrenome";
            this.Sobrenome.Name = "Sobrenome";
            this.Sobrenome.ReadOnly = true;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "nm_nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // Funcionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(1030, 592);
            this.Controls.Add(this.tcCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Funcionarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroFuncionarios";
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarFuncionarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.Cadastrar.ResumeLayout(false);
            this.Cadastrar.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tcCadastrarFuncionario.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.DataGridView dgvConsultarFuncionarios;
        private System.Windows.Forms.TextBox txtConsultarFuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnConsultarFuncionario;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage Cadastrar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox mtbCepFuncionario;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox mtbTelefonemóvelFuncionario;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNcomplementoFuncionario;
        private System.Windows.Forms.MaskedTextBox mtbTelefoneFuncionario;
        private System.Windows.Forms.TextBox txtNcasaFuncionario;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtSobrenomeFuncionario;
        private System.Windows.Forms.TextBox TxtNomeFuncionario;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbDepartamentoFuncionario;
        private System.Windows.Forms.MaskedTextBox mtbCpfFuncionario;
        private System.Windows.Forms.MaskedTextBox mtbRgFuncionario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpNascimentoFuncionario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox mtbCarteiraFuncionario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmailFuncionario;
        private System.Windows.Forms.TextBox txtLoginFuncionario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSenhaFuncionario;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnSalvarFuncionario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tcCadastrarFuncionario;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtsalariobruto;
        private System.Windows.Forms.RadioButton rdnvendedor;
        private System.Windows.Forms.RadioButton rbnADM;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Departamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sobrenome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
    }
}