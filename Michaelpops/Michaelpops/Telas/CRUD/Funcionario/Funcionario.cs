﻿using Michaelpops.Programação;
using Michaelpops.Programação.Departamento__só_pra_combo_;
using Michaelpops.Programação.Funcionario;
using Michaelpops.Telas.CRUD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class Funcionarios : Form
    {
        Validação v = new Validação();
        public Funcionarios()
        {
            InitializeComponent();
            VerificarPermissoes();
            CarregarCombos();
            
        }
        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.bt_permissaoadm == false)
            {
              btnalterar.Enabled = false;
                
               
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Cadastrar_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void TxtNomeFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtNcomplementoFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
   
        }

        private void txtEmailFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void BtnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            try
            {
                

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.nm_nome = TxtNomeFuncionario.Text;
                dto.nm_sobrenome = TxtSobrenomeFuncionario.Text;
                dto.ds_cpf = mtbCpfFuncionario.Text;
                dto.ds_rg = mtbRgFuncionario.Text;
                dto.dt_nascimento = dtpNascimentoFuncionario.Value;
                dto.ds_email = txtEmailFuncionario.Text;
                dto.ds_login = txtLoginFuncionario.Text;
                dto.ds_senha = txtSenhaFuncionario.Text;
                dto.ds_dpto = cbDepartamentoFuncionario.Text;
                dto.ds_carteiratrabalho = mtbCarteiraFuncionario.Text;
                dto.ds_telefone = mtbTelefoneFuncionario.Text;
                dto.ds_telefonemovel = mtbTelefonemóvelFuncionario.Text;
                dto.ds_cep = mtbCepFuncionario.Text;
                dto.ds_complemento = txtNcomplementoFuncionario.Text;
                dto.ds_ncasa = txtNcasaFuncionario.Text;
                dto.vl_salariobruto = Convert.ToDecimal(txtsalariobruto.Text);
                dto.bt_permissaoadm = rbnADM.Checked;
                dto.bt_permissaofuncionario = rdnvendedor.Checked;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);
                MessageBox.Show("Funcionário salvo com sucesso!");
                TxtNomeFuncionario.Text = "";
                TxtSobrenomeFuncionario.Text = "";
                mtbCpfFuncionario.Text = "";
                mtbRgFuncionario.Text = "";
                txtEmailFuncionario.Text = "";
                txtLoginFuncionario.Text = "";
                txtSenhaFuncionario.Text = "";
                cbDepartamentoFuncionario.Text = "";
                mtbCarteiraFuncionario.Text = "";
                mtbTelefoneFuncionario.Text = "";
                mtbTelefonemóvelFuncionario.Text = "";
                mtbCepFuncionario.Text = "";
                txtNcomplementoFuncionario.Text = "";
                txtNcasaFuncionario.Text = "";
                txtsalariobruto.Clear();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro " + ex.Message);
            }


        }

        private void BtnConsultarFuncionario_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> a = business.Consultar(txtConsultarFuncionario.Text);
            dgvConsultarFuncionarios.AutoGenerateColumns = false;
            dgvConsultarFuncionarios.DataSource = a;
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
             if(dgvConsultarFuncionarios.CurrentRow != null)
                {
                    FuncionarioDTO funcionario = dgvConsultarFuncionarios.CurrentRow.DataBoundItem as FuncionarioDTO;

                    AlterarFuncionario tela = new AlterarFuncionario();
                    tela.LoadScreen(funcionario);
                    tela.Show();
                    this.Hide();

                }
             else
                {
                    MessageBox.Show("Selecione um funcionario");

                }



            }
            
            catch (Exception ex)
            {
                
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgvConsultarFuncionarios.CurrentRow != null)
                {
                    FuncionarioDTO funcionario = dgvConsultarFuncionarios.CurrentRow.DataBoundItem as FuncionarioDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse funcionário?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);

                    if (funcionario.id_funcionario != UserSession.UsuarioLogado.id_funcionario)
                    {
                        if (r == DialogResult.Yes)
                        {
                            FuncionarioBusiness business = new FuncionarioBusiness();
                            business.Remover(funcionario.id_funcionario);

                            List<FuncionarioDTO> a = business.Consultar(txtConsultarFuncionario.Text);
                            dgvConsultarFuncionarios.AutoGenerateColumns = false;
                            dgvConsultarFuncionarios.DataSource = a;

                        }
                    }
                    else
                    {
                        MessageBox.Show("Não pode deletar um usuário que está logado");
                    }
                }
                else
                {
                    MessageBox.Show("Selecione um funcionario");
                }
                
               
                
               
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }


        }

        private void cbDepartamentoFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        void CarregarCombos()
        {
            DptoBusiness bus = new DptoBusiness();
            List<DptoDTO> lista = bus.Listar();
            cbDepartamentoFuncionario.DisplayMember = nameof(DptoDTO.nm_nome);
            cbDepartamentoFuncionario.DataSource = lista;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void txtteste_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNcomplementoFuncionario_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtNcomplementoFuncionario_MouseClick(object sender, MouseEventArgs e)
        {
            txtNcomplementoFuncionario.Clear();
        }

        private void txtsalariobruto_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtsalariobruto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
