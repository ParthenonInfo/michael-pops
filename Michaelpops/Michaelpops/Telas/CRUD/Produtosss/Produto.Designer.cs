﻿namespace Michaelpops.Telas
{
    partial class Produto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Produto));
            this.tcCadastrarFuncionario = new System.Windows.Forms.TabControl();
            this.Cadastrar = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVlProduto = new System.Windows.Forms.TextBox();
            this.txtUnidade = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtNomeProduto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnSalvarProduto = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnalterar = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.dgvConsultarProdutos = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtConsultarProduto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnConsultarProduto = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tcCadastrarFuncionario.SuspendLayout();
            this.Cadastrar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarProdutos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tcCadastrarFuncionario
            // 
            this.tcCadastrarFuncionario.Controls.Add(this.Cadastrar);
            this.tcCadastrarFuncionario.Controls.Add(this.tabPage2);
            this.tcCadastrarFuncionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcCadastrarFuncionario.Location = new System.Drawing.Point(12, 17);
            this.tcCadastrarFuncionario.Name = "tcCadastrarFuncionario";
            this.tcCadastrarFuncionario.SelectedIndex = 0;
            this.tcCadastrarFuncionario.Size = new System.Drawing.Size(1006, 558);
            this.tcCadastrarFuncionario.TabIndex = 36;
            // 
            // Cadastrar
            // 
            this.Cadastrar.BackColor = System.Drawing.Color.Snow;
            this.Cadastrar.Controls.Add(this.groupBox1);
            this.Cadastrar.Controls.Add(this.pictureBox1);
            this.Cadastrar.Controls.Add(this.BtnSalvarProduto);
            this.Cadastrar.Controls.Add(this.label2);
            this.Cadastrar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Cadastrar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cadastrar.Location = new System.Drawing.Point(4, 26);
            this.Cadastrar.Name = "Cadastrar";
            this.Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.Cadastrar.Size = new System.Drawing.Size(998, 528);
            this.Cadastrar.TabIndex = 0;
            this.Cadastrar.Text = "Cadastrar";
            this.Cadastrar.Click += new System.EventHandler(this.Cadastrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtVlProduto);
            this.groupBox1.Controls.Add(this.txtUnidade);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtNomeProduto);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(6, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 296);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(45, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Preço";
            // 
            // txtVlProduto
            // 
            this.txtVlProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVlProduto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlProduto.Location = new System.Drawing.Point(105, 61);
            this.txtVlProduto.MaxLength = 30;
            this.txtVlProduto.Name = "txtVlProduto";
            this.txtVlProduto.Size = new System.Drawing.Size(190, 21);
            this.txtVlProduto.TabIndex = 2;
            this.txtVlProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlProduto_KeyPress);
            // 
            // txtUnidade
            // 
            this.txtUnidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUnidade.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnidade.Location = new System.Drawing.Point(105, 98);
            this.txtUnidade.MaxLength = 300;
            this.txtUnidade.Multiline = true;
            this.txtUnidade.Name = "txtUnidade";
            this.txtUnidade.Size = new System.Drawing.Size(190, 181);
            this.txtUnidade.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nome";
            // 
            // TxtNomeProduto
            // 
            this.TxtNomeProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNomeProduto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNomeProduto.Location = new System.Drawing.Point(105, 22);
            this.TxtNomeProduto.MaxLength = 30;
            this.TxtNomeProduto.Name = "TxtNomeProduto";
            this.TxtNomeProduto.Size = new System.Drawing.Size(190, 21);
            this.TxtNomeProduto.TabIndex = 1;
            this.TxtNomeProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNomeProduto_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Descrição";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(926, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // BtnSalvarProduto
            // 
            this.BtnSalvarProduto.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalvarProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSalvarProduto.FlatAppearance.BorderSize = 0;
            this.BtnSalvarProduto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnSalvarProduto.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalvarProduto.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalvarProduto.Image")));
            this.BtnSalvarProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSalvarProduto.Location = new System.Drawing.Point(870, 483);
            this.BtnSalvarProduto.Name = "BtnSalvarProduto";
            this.BtnSalvarProduto.Size = new System.Drawing.Size(122, 39);
            this.BtnSalvarProduto.TabIndex = 2;
            this.BtnSalvarProduto.Text = "Salvar";
            this.BtnSalvarProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSalvarProduto.UseVisualStyleBackColor = false;
            this.BtnSalvarProduto.Click += new System.EventHandler(this.BtnSalvarProduto_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 36);
            this.label2.TabIndex = 4;
            this.label2.Text = "Produto";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Snow;
            this.tabPage2.Controls.Add(this.btnalterar);
            this.tabPage2.Controls.Add(this.btnApagar);
            this.tabPage2.Controls.Add(this.dgvConsultarProdutos);
            this.tabPage2.Controls.Add(this.txtConsultarProduto);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.BtnConsultarProduto);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(998, 528);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consultar/Apagar/Alterar";
            // 
            // btnalterar
            // 
            this.btnalterar.BackColor = System.Drawing.Color.Transparent;
            this.btnalterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnalterar.FlatAppearance.BorderSize = 0;
            this.btnalterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnalterar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnalterar.Image = ((System.Drawing.Image)(resources.GetObject("btnalterar.Image")));
            this.btnalterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnalterar.Location = new System.Drawing.Point(764, 43);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(119, 39);
            this.btnalterar.TabIndex = 21;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnalterar.UseVisualStyleBackColor = false;
            this.btnalterar.Click += new System.EventHandler(this.btnalterar_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.BackColor = System.Drawing.Color.Transparent;
            this.btnApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApagar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApagar.Location = new System.Drawing.Point(643, 42);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(115, 39);
            this.btnApagar.TabIndex = 20;
            this.btnApagar.Text = "Apagar";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // dgvConsultarProdutos
            // 
            this.dgvConsultarProdutos.AllowUserToAddRows = false;
            this.dgvConsultarProdutos.AllowUserToDeleteRows = false;
            this.dgvConsultarProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Descrição});
            this.dgvConsultarProdutos.Location = new System.Drawing.Point(21, 87);
            this.dgvConsultarProdutos.Name = "dgvConsultarProdutos";
            this.dgvConsultarProdutos.ReadOnly = true;
            this.dgvConsultarProdutos.RowHeadersVisible = false;
            this.dgvConsultarProdutos.Size = new System.Drawing.Size(960, 434);
            this.dgvConsultarProdutos.TabIndex = 17;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "nm_nome";
            this.Column1.HeaderText = "Nome";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "vl_produto";
            this.Column3.HeaderText = "Valor";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // Descrição
            // 
            this.Descrição.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descrição.DataPropertyName = "ds_descricao";
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            // 
            // txtConsultarProduto
            // 
            this.txtConsultarProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConsultarProduto.Location = new System.Drawing.Point(21, 59);
            this.txtConsultarProduto.MaxLength = 50;
            this.txtConsultarProduto.Name = "txtConsultarProduto";
            this.txtConsultarProduto.Size = new System.Drawing.Size(479, 23);
            this.txtConsultarProduto.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 36);
            this.label1.TabIndex = 6;
            this.label1.Text = "Produto";
            // 
            // BtnConsultarProduto
            // 
            this.BtnConsultarProduto.BackColor = System.Drawing.Color.Transparent;
            this.BtnConsultarProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnConsultarProduto.FlatAppearance.BorderSize = 0;
            this.BtnConsultarProduto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnConsultarProduto.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConsultarProduto.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultarProduto.Image")));
            this.BtnConsultarProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnConsultarProduto.Location = new System.Drawing.Point(506, 42);
            this.BtnConsultarProduto.Name = "BtnConsultarProduto";
            this.BtnConsultarProduto.Size = new System.Drawing.Size(131, 39);
            this.BtnConsultarProduto.TabIndex = 5;
            this.BtnConsultarProduto.Text = "Consultar";
            this.BtnConsultarProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnConsultarProduto.UseVisualStyleBackColor = false;
            this.BtnConsultarProduto.Click += new System.EventHandler(this.BtnConsultarProduto_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(926, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // Produto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(1030, 592);
            this.Controls.Add(this.tcCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Produto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Produto";
            this.tcCadastrarFuncionario.ResumeLayout(false);
            this.Cadastrar.ResumeLayout(false);
            this.Cadastrar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarProdutos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcCadastrarFuncionario;
        private System.Windows.Forms.TabPage Cadastrar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtNomeProduto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnSalvarProduto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvConsultarProdutos;
        private System.Windows.Forms.TextBox txtConsultarProduto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnConsultarProduto;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.TextBox txtUnidade;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVlProduto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
    }
}