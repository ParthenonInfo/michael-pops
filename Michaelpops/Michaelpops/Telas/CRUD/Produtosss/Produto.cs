﻿using Michaelpops.Programação;
using Michaelpops.Programação.Produto;
using Michaelpops.Telas.Compras;
using Michaelpops.Telas.CRUD.Produtosss;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class Produto : Form
    {
        Validação v = new Validação();
        public Produto()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void Cadastrar_Click(object sender, EventArgs e)
        {

        }

        private void TxtNomeProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtUnidadeProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void BtnSalvarProduto_Click(object sender, EventArgs e)
        {
            try
            {

                ProdutoDTO dto = new ProdutoDTO();
                dto.nm_nome = TxtNomeProduto.Text;
                dto.ds_descricao = txtUnidade.Text;
                dto.vl_produto = Convert.ToDecimal(txtVlProduto.Text);


                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um  erro: " + ex.Message);
            }
        }

        private void BtnConsultarProduto_Click(object sender, EventArgs e)
        {
            ProdutoBusiness Business = new ProdutoBusiness();
            List<ProdutoDTO> a = Business.Consultar(txtConsultarProduto.Text);
            dgvConsultarProdutos.AutoGenerateColumns = false;
            dgvConsultarProdutos.DataSource = a;
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarProdutos.CurrentRow != null)
                {
                    ProdutoDTO fornecedor = dgvConsultarProdutos.CurrentRow.DataBoundItem as ProdutoDTO;

                    AlterarProduto tela = new AlterarProduto();
                    tela.LoadScreen(fornecedor);
                    tela.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Selecione um Produto");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
          
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarProdutos.CurrentRow != null)
                {
                    ProdutoDTO produto = dgvConsultarProdutos.CurrentRow.DataBoundItem as ProdutoDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir esse Produto?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        ProdutoBusiness business = new ProdutoBusiness();
                        business.Remover(produto.id_produto);

                        List<ProdutoDTO> a = business.Consultar(txtConsultarProduto.Text);
                        dgvConsultarProdutos.AutoGenerateColumns = false;
                        dgvConsultarProdutos.DataSource = a;

                    }
                    else
                    {
                        MessageBox.Show("Selecione um Produto.");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CadastrarCompra tela = new CadastrarCompra();
            tela.Show();
            this.Hide();
        }

        private void txtVlProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
