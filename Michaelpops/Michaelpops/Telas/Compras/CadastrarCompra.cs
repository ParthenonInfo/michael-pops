﻿using Michaelpops.Programação;
using Michaelpops.Programação.Compras;
using Michaelpops.Programação.Entregável_2.Compras;
using Michaelpops.Programação.Entregável_2.Compras.ProdutoCompra;
using Michaelpops.Programação.Estoque;
using Michaelpops.Programação.Fornecedor;
using Michaelpops.Programação.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas.Compras
{
    public partial class CadastrarCompra : Form
    {
        BindingList<Programação.Entregável_2.Compras.ProdutoCompra.ProdutoCompraDTO> produtosCarrinho = new BindingList<Programação.Entregável_2.Compras.ProdutoCompra.ProdutoCompraDTO>();
        decimal valortotal = 0;
        Validação V = new Validação();
        public CadastrarCompra()
        {
            InitializeComponent();
            CarregarCombos();
        }
       public void CarregarCombos()
        {
            FornecedorBusiness bus = new FornecedorBusiness();
            List<FornecedorDTO> lista = bus.Listar();
            cbfornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cbfornecedor.DisplayMember = nameof(FornecedorDTO.nm_nome);
            cbfornecedor.DataSource = lista;

            ProdutoCompraBusiness busi = new ProdutoCompraBusiness();
            List<Programação.Entregável_2.Compras.ProdutoCompra.ProdutoCompraDTO> list = busi.Consultar(string.Empty);
            cmbCompra.DisplayMember = nameof(ProdutoCompraDTO.nm_produtocompra);
            cmbCompra.ValueMember = nameof(ProdutoCompraDTO.id_produtocompra);
            cmbCompra.DataSource = list;

        }

        CompraItemDTO compraaaaaitemmmm = new CompraItemDTO();

        private void BtnSalvarFluxoCaixa_Click(object sender, EventArgs e)
        {/*
            try
            {
                CompraItemBusiness compraaaaaiteeeeembusineeeees = new CompraItemBusiness();
               // int quantidade = int.Parse(txtqtd.Text);
                FornecedorDTO fornecedor = cbfornecedor.SelectedItem as FornecedorDTO;
                ComprasDTO dto = new ComprasDTO();
                dto.id_fornecedor = fornecedor.id_fornecedor;
                dto.dt_compra = DateTime.Now;

                ComprasBusiness bussiness = new ComprasBusiness();
                bussiness.Salvar(dto);

                EstoqueBusiness businessestoque = new EstoqueBusiness();
               // List<VwConsultarItem> lista = compraaaaaiteeeeembusineeeees.ConsultarView(txtNome.Text);
                List<EstoqueDTO> estoque = new List<EstoqueDTO>();

                //foreach (VwConsultarItem item in lista)
                //{
                //    foreach (EstoqueDTO item2 in estoque)
                //    {
                //        if (item.id_compra == item2.id_compra)
                //        {
                //            item2.Quantidade += item.qtd_itens;
                //        }
                //    }
                //}


                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.id_produtocompra = item.id_produtocompra;
                    estoquedto.ds_quantidade = item.ds_quantidade;

                    businessestoque.Salvar(estoquedto);
                }
                
                MessageBox.Show("Compra efetuada com sucesso");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro " + ex.Message);
            }

        */}

            private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();

        }



        private void BtnConsultarFluxoCaixa_Click(object sender, EventArgs e)
        {
            try
            {
                CompraItemBusiness business = new CompraItemBusiness();
                List<VwConsultarItem> a = business.ConsultarView(txtConsultarCompra.Text);
                dgvConsultarCompra.AutoGenerateColumns = false;
                dgvConsultarCompra.DataSource = a;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }



        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvConsultarCompra.CurrentRow != null)
                {
                    ComprasDTO compra = dgvConsultarCompra.CurrentRow.DataBoundItem as ComprasDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir essa compra?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);

                    if (compra.id_compra != UserSession.UsuarioLogado.id_funcionario)
                    {
                        if (r == DialogResult.Yes)
                        {
                            ComprasBusiness business = new ComprasBusiness();
                            business.Remover(compra.id_compra);

                            List<ComprasDTO> a = business.Consultar(txtConsultarCompra.Text);
                            dgvConsultarCompra.AutoGenerateColumns = false;
                            dgvConsultarCompra.DataSource = a;

                        }
                    }

                }
                else
                {
                    MessageBox.Show("Selecione uma Compra");
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }


        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void txtVlcompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

     

        private void button2_Click_1(object sender, EventArgs e)
        {
            produtosCarrinho = new BindingList<ProdutoCompraDTO>();
            valortotal = 0;
            lblvalortotal.Text = "R$ " + valortotal.ToString();
            dataGridView1.DataSource = produtosCarrinho;
            txtQuantidade.Clear();
            compraaaaaitemmmm = new CompraItemDTO();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = cbfornecedor.SelectedItem as FornecedorDTO;
                ComprasDTO compra = new ComprasDTO();
                compra.dt_compra = DateTime.Now;
                compra.id_fornecedor = fornecedor.id_fornecedor;
                
               

                
                ComprasBusiness business = new ComprasBusiness();
                int idcompra = business.Salvar(compra, produtosCarrinho.ToList());

                MessageBox.Show("Compra realizada com sucesso");

                produtosCarrinho = new BindingList<ProdutoCompraDTO>();
                valortotal = 0;
                lblvalortotal.Text = "R$ " + valortotal.ToString();
                dataGridView1.DataSource = produtosCarrinho;
                txtQuantidade.Clear();
                EstoqueBusiness businessestoque = new EstoqueBusiness();
                CompraItemBusiness compraItemBusiness = new CompraItemBusiness();
                List<VwConsultarItem> lista = compraItemBusiness.ConsultarViewPorId(idcompra);
                List<EstoqueDTO> estoque = businessestoque.Listar();


                foreach (VwConsultarItem item in lista)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if (item.id_produtocompra == item2.id_produtocompra)
                        {
                            item2.ds_quantidade = item2.ds_quantidade + item.qtd_itens;
                        }
                    }
                }


                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.id_produtocompra = item.id_produtocompra;
                    estoquedto.ds_quantidade = item.ds_quantidade;

                    businessestoque.Alterar(estoquedto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ocorreu o erro: " + ex.Message);
            }
        }

        private void tcCadastrarFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarCombos();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoCompraBusiness business = new ProdutoCompraBusiness();
                List<ProdutoCompraDTO> a = business.Consultar(txtConsultarCompra.Text);
                dgvItensProdutos.AutoGenerateColumns = false;
                dgvItensProdutos.DataSource = a;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ProdutoCompraDTO dto = new ProdutoCompraDTO();
            dto.nm_produtocompra = txtProdutoComprado.Text;
            dto.vl_valor = decimal.Parse(txtValor.Text);
            ProdutoCompraBusiness business = new ProdutoCompraBusiness();
            int idproduto = business.Salvar(dto);

            EstoqueDTO estoquedto = new EstoqueDTO();
            estoquedto.id_produtocompra = idproduto;
            estoquedto.ds_quantidade = 0;
            EstoqueBusiness estoque = new EstoqueBusiness();
            estoque.Salvar(estoquedto);
            MessageBox.Show("Produto de compra cadastrado com sucesso");
            txtProdutoComprado.Clear();
            txtValor.Clear();

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
        
        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                int quantidade = 0;
                ProdutoCompraDTO dto = cmbCompra.SelectedItem as ProdutoCompraDTO;
                int qtd = Convert.ToInt32(txtQuantidade.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                    dataGridView1.AutoGenerateColumns = false;
                    dataGridView1.DataSource = produtosCarrinho;
                    valortotal = valortotal + dto.vl_valor;
                    quantidade = quantidade + 1;

                }
                lblvalortotal.Text = "R$ " + valortotal.ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
 
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnapagarprodutocompra_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvItensProdutos.CurrentRow != null)
                {
                    ProdutoCompraDTO compra = dgvItensProdutos.CurrentRow.DataBoundItem as ProdutoCompraDTO;
                    DialogResult r = MessageBox.Show("Deseja excluir essa compra?", "Michael Pop`s",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question);

                   
                    
                        if (r == DialogResult.Yes)
                        {
                            ProdutoCompraBusiness business = new ProdutoCompraBusiness();
                            business.Remover(compra.id_produtocompra);

                            List<ProdutoCompraDTO> a = business.Consultar(txtconsultarprodutocompra.Text);
                            dgvConsultarCompra.AutoGenerateColumns = false;
                            dgvConsultarCompra.DataSource = a;

                        }
                    

                }
                else
                {
                    MessageBox.Show("Selecione uma Compra");
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }

        }

        private void lblvalortotal_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click_2(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }
    }
}

