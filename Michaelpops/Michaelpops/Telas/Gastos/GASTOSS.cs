﻿using Michaelpops.Programação;
using Michaelpops.Programação.Entregável_4.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas.Gastos
{
    public partial class GASTOSS : Form
    {
        Validação v = new Validação();
        public GASTOSS()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }

        private void BtnSalvarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                GastoDTO dto = new GastoDTO();

                dto.Gasto = txtnome.Text;
                dto.Tipo = cboTipo.SelectedItem.ToString();
                dto.Pagamento = dtppagamento.Value;
                dto.Valor = Convert.ToDecimal(txtvalor.Text);

                GastoBusiness business = new GastoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Gasto salvo com sucesso");
                txtnome.Clear();
                txtvalor.Clear();
                dtppagamento.Value = DateTime.Now;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnConsultarCliente_Click(object sender, EventArgs e)
        {
              GastoBusiness business = new GastoBusiness();
            List<GastoDTO> a =business.Consultar(txtConsultarCliente.Text);
            dgvconsultaestoq.AutoGenerateColumns = false;
            dgvconsultaestoq.DataSource = a;
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }
    }
}
