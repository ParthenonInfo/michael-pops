﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops.Telas
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(6000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    Login frm = new Login();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}

