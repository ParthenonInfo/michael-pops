﻿namespace Michaelpops
{
    partial class Menu
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btncompras = new System.Windows.Forms.Button();
            this.pbLogoff = new System.Windows.Forms.PictureBox();
            this.pbSair = new System.Windows.Forms.PictureBox();
            this.BtnFornecedor = new System.Windows.Forms.Button();
            this.btnClientes = new System.Windows.Forms.Button();
            this.BtnFolhadepagamento = new System.Windows.Forms.Button();
            this.BtnFuncionarios = new System.Windows.Forms.Button();
            this.BtnFluxocaixa = new System.Windows.Forms.Button();
            this.BtnEstoque = new System.Windows.Forms.Button();
            this.BtnVendas = new System.Windows.Forms.Button();
            this.BtnPedido = new System.Windows.Forms.Button();
            this.BtnProduto = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnHistoria = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnSobre = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.fader = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSair)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Firebrick;
            this.panel2.Controls.Add(this.btncompras);
            this.panel2.Controls.Add(this.pbLogoff);
            this.panel2.Controls.Add(this.pbSair);
            this.panel2.Controls.Add(this.BtnFornecedor);
            this.panel2.Controls.Add(this.btnClientes);
            this.panel2.Controls.Add(this.BtnFolhadepagamento);
            this.panel2.Controls.Add(this.BtnFuncionarios);
            this.panel2.Controls.Add(this.BtnFluxocaixa);
            this.panel2.Controls.Add(this.BtnEstoque);
            this.panel2.Controls.Add(this.BtnVendas);
            this.panel2.Controls.Add(this.BtnPedido);
            this.panel2.Controls.Add(this.BtnProduto);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1087, 69);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // btncompras
            // 
            this.btncompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncompras.FlatAppearance.BorderSize = 0;
            this.btncompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncompras.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncompras.ForeColor = System.Drawing.Color.Transparent;
            this.btncompras.Image = ((System.Drawing.Image)(resources.GetObject("btncompras.Image")));
            this.btncompras.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btncompras.Location = new System.Drawing.Point(213, 3);
            this.btncompras.Name = "btncompras";
            this.btncompras.Size = new System.Drawing.Size(86, 66);
            this.btncompras.TabIndex = 3;
            this.btncompras.Text = "Compra";
            this.btncompras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btncompras.UseVisualStyleBackColor = true;
            this.btncompras.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pbLogoff
            // 
            this.pbLogoff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbLogoff.Image = ((System.Drawing.Image)(resources.GetObject("pbLogoff.Image")));
            this.pbLogoff.Location = new System.Drawing.Point(990, 12);
            this.pbLogoff.Name = "pbLogoff";
            this.pbLogoff.Size = new System.Drawing.Size(44, 43);
            this.pbLogoff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogoff.TabIndex = 13;
            this.pbLogoff.TabStop = false;
            this.pbLogoff.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pbSair
            // 
            this.pbSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbSair.Image = ((System.Drawing.Image)(resources.GetObject("pbSair.Image")));
            this.pbSair.Location = new System.Drawing.Point(1040, 12);
            this.pbSair.Name = "pbSair";
            this.pbSair.Size = new System.Drawing.Size(44, 43);
            this.pbSair.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSair.TabIndex = 12;
            this.pbSair.TabStop = false;
            this.pbSair.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BtnFornecedor
            // 
            this.BtnFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnFornecedor.FlatAppearance.BorderSize = 0;
            this.BtnFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFornecedor.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFornecedor.ForeColor = System.Drawing.Color.Transparent;
            this.BtnFornecedor.Image = ((System.Drawing.Image)(resources.GetObject("BtnFornecedor.Image")));
            this.BtnFornecedor.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnFornecedor.Location = new System.Drawing.Point(305, 3);
            this.BtnFornecedor.Name = "BtnFornecedor";
            this.BtnFornecedor.Size = new System.Drawing.Size(98, 63);
            this.BtnFornecedor.TabIndex = 4;
            this.BtnFornecedor.Text = "Fornecedor";
            this.BtnFornecedor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnFornecedor.UseVisualStyleBackColor = true;
            this.BtnFornecedor.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClientes.FlatAppearance.BorderSize = 0;
            this.btnClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClientes.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClientes.ForeColor = System.Drawing.Color.Transparent;
            this.btnClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnClientes.Image")));
            this.btnClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClientes.Location = new System.Drawing.Point(502, 1);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Size = new System.Drawing.Size(86, 65);
            this.btnClientes.TabIndex = 6;
            this.btnClientes.Text = "Cliente";
            this.btnClientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.button11_Click);
            // 
            // BtnFolhadepagamento
            // 
            this.BtnFolhadepagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnFolhadepagamento.FlatAppearance.BorderSize = 0;
            this.BtnFolhadepagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFolhadepagamento.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFolhadepagamento.ForeColor = System.Drawing.Color.Transparent;
            this.BtnFolhadepagamento.Image = ((System.Drawing.Image)(resources.GetObject("BtnFolhadepagamento.Image")));
            this.BtnFolhadepagamento.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnFolhadepagamento.Location = new System.Drawing.Point(115, 3);
            this.BtnFolhadepagamento.Name = "BtnFolhadepagamento";
            this.BtnFolhadepagamento.Size = new System.Drawing.Size(92, 65);
            this.BtnFolhadepagamento.TabIndex = 2;
            this.BtnFolhadepagamento.Text = "Folha de pagamento";
            this.BtnFolhadepagamento.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnFolhadepagamento.UseVisualStyleBackColor = true;
            this.BtnFolhadepagamento.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnFuncionarios
            // 
            this.BtnFuncionarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnFuncionarios.FlatAppearance.BorderSize = 0;
            this.BtnFuncionarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFuncionarios.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFuncionarios.ForeColor = System.Drawing.Color.Transparent;
            this.BtnFuncionarios.Image = ((System.Drawing.Image)(resources.GetObject("BtnFuncionarios.Image")));
            this.BtnFuncionarios.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnFuncionarios.Location = new System.Drawing.Point(3, 0);
            this.BtnFuncionarios.Name = "BtnFuncionarios";
            this.BtnFuncionarios.Size = new System.Drawing.Size(111, 72);
            this.BtnFuncionarios.TabIndex = 1;
            this.BtnFuncionarios.Text = "Funcionário";
            this.BtnFuncionarios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnFuncionarios.UseVisualStyleBackColor = true;
            this.BtnFuncionarios.Click += new System.EventHandler(this.button10_Click);
            // 
            // BtnFluxocaixa
            // 
            this.BtnFluxocaixa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnFluxocaixa.FlatAppearance.BorderSize = 0;
            this.BtnFluxocaixa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFluxocaixa.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFluxocaixa.ForeColor = System.Drawing.Color.Transparent;
            this.BtnFluxocaixa.Image = ((System.Drawing.Image)(resources.GetObject("BtnFluxocaixa.Image")));
            this.BtnFluxocaixa.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnFluxocaixa.Location = new System.Drawing.Point(886, 2);
            this.BtnFluxocaixa.Name = "BtnFluxocaixa";
            this.BtnFluxocaixa.Size = new System.Drawing.Size(98, 66);
            this.BtnFluxocaixa.TabIndex = 10;
            this.BtnFluxocaixa.Text = "Fluxo de \r\nCaixa";
            this.BtnFluxocaixa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnFluxocaixa.UseVisualStyleBackColor = true;
            this.BtnFluxocaixa.Click += new System.EventHandler(this.button9_Click);
            // 
            // BtnEstoque
            // 
            this.BtnEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEstoque.FlatAppearance.BorderSize = 0;
            this.BtnEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEstoque.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEstoque.ForeColor = System.Drawing.Color.Transparent;
            this.BtnEstoque.Image = ((System.Drawing.Image)(resources.GetObject("BtnEstoque.Image")));
            this.BtnEstoque.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnEstoque.Location = new System.Drawing.Point(778, -1);
            this.BtnEstoque.Name = "BtnEstoque";
            this.BtnEstoque.Size = new System.Drawing.Size(92, 72);
            this.BtnEstoque.TabIndex = 9;
            this.BtnEstoque.Text = "Estoque";
            this.BtnEstoque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnEstoque.UseVisualStyleBackColor = true;
            this.BtnEstoque.Click += new System.EventHandler(this.button8_Click);
            // 
            // BtnVendas
            // 
            this.BtnVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnVendas.FlatAppearance.BorderSize = 0;
            this.BtnVendas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnVendas.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVendas.ForeColor = System.Drawing.Color.Transparent;
            this.BtnVendas.Image = ((System.Drawing.Image)(resources.GetObject("BtnVendas.Image")));
            this.BtnVendas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnVendas.Location = new System.Drawing.Point(686, 5);
            this.BtnVendas.Name = "BtnVendas";
            this.BtnVendas.Size = new System.Drawing.Size(86, 65);
            this.BtnVendas.TabIndex = 8;
            this.BtnVendas.Text = "Gasto";
            this.BtnVendas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnVendas.UseVisualStyleBackColor = true;
            this.BtnVendas.Click += new System.EventHandler(this.button7_Click);
            // 
            // BtnPedido
            // 
            this.BtnPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnPedido.FlatAppearance.BorderSize = 0;
            this.BtnPedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPedido.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPedido.ForeColor = System.Drawing.Color.Transparent;
            this.BtnPedido.Image = ((System.Drawing.Image)(resources.GetObject("BtnPedido.Image")));
            this.BtnPedido.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnPedido.Location = new System.Drawing.Point(594, 3);
            this.BtnPedido.Name = "BtnPedido";
            this.BtnPedido.Size = new System.Drawing.Size(86, 65);
            this.BtnPedido.TabIndex = 7;
            this.BtnPedido.Text = "Pedido";
            this.BtnPedido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPedido.UseVisualStyleBackColor = true;
            this.BtnPedido.Click += new System.EventHandler(this.button4_Click);
            // 
            // BtnProduto
            // 
            this.BtnProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnProduto.FlatAppearance.BorderSize = 0;
            this.BtnProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnProduto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProduto.ForeColor = System.Drawing.Color.Transparent;
            this.BtnProduto.Image = ((System.Drawing.Image)(resources.GetObject("BtnProduto.Image")));
            this.BtnProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnProduto.Location = new System.Drawing.Point(410, 1);
            this.BtnProduto.Name = "BtnProduto";
            this.BtnProduto.Size = new System.Drawing.Size(86, 65);
            this.BtnProduto.TabIndex = 5;
            this.BtnProduto.Text = "Produto";
            this.BtnProduto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnProduto.UseVisualStyleBackColor = true;
            this.BtnProduto.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Controls.Add(this.BtnHistoria);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(139, 195);
            this.panel1.TabIndex = 2;
            // 
            // BtnHistoria
            // 
            this.BtnHistoria.BackColor = System.Drawing.Color.IndianRed;
            this.BtnHistoria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnHistoria.FlatAppearance.BorderSize = 0;
            this.BtnHistoria.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnHistoria.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnHistoria.ForeColor = System.Drawing.Color.Black;
            this.BtnHistoria.Location = new System.Drawing.Point(3, 134);
            this.BtnHistoria.Name = "BtnHistoria";
            this.BtnHistoria.Size = new System.Drawing.Size(133, 58);
            this.BtnHistoria.TabIndex = 15;
            this.BtnHistoria.Text = "Conheça nossa história!";
            this.BtnHistoria.UseVisualStyleBackColor = false;
            this.BtnHistoria.Click += new System.EventHandler(this.button12_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 131);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-4, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Michael Pop´s";
            // 
            // BtnSobre
            // 
            this.BtnSobre.BackColor = System.Drawing.Color.Maroon;
            this.BtnSobre.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSobre.FlatAppearance.BorderSize = 0;
            this.BtnSobre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSobre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSobre.Image = ((System.Drawing.Image)(resources.GetObject("BtnSobre.Image")));
            this.BtnSobre.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnSobre.Location = new System.Drawing.Point(990, 75);
            this.BtnSobre.Name = "BtnSobre";
            this.BtnSobre.Size = new System.Drawing.Size(85, 40);
            this.BtnSobre.TabIndex = 16;
            this.BtnSobre.Text = "Sobre";
            this.BtnSobre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSobre.UseVisualStyleBackColor = false;
            this.BtnSobre.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(145, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 78);
            this.label2.TabIndex = 4;
            this.label2.Text = "Menu";
            // 
            // fader
            // 
            this.fader.Tick += new System.EventHandler(this.fader_Tick);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Firebrick;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.SeaShell;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(158, 201);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(164, 56);
            this.button2.TabIndex = 11;
            this.button2.Text = "Enviar Email";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1087, 592);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnSobre);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSair)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button BtnFuncionarios;
        private System.Windows.Forms.Button BtnFluxocaixa;
        private System.Windows.Forms.Button BtnEstoque;
        private System.Windows.Forms.Button BtnVendas;
        private System.Windows.Forms.Button BtnPedido;
        private System.Windows.Forms.Button BtnProduto;
        private System.Windows.Forms.Button BtnFornecedor;
        private System.Windows.Forms.Button BtnFolhadepagamento;
        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.PictureBox pbSair;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbLogoff;
        private System.Windows.Forms.Button BtnHistoria;
        private System.Windows.Forms.Button BtnSobre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btncompras;
        private System.Windows.Forms.Timer fader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
    }
}

