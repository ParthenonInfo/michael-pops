﻿
using Michaelpops.Telas;
using Michaelpops.Telas.API;
using Michaelpops.Telas.Compras;
using Michaelpops.Telas.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Michaelpops
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            VerificarPermissoes();

        }
        void VerificarPermissoes()
        {
            if(UserSession.UsuarioLogado.bt_permissaoadm == false)
            {
                if(UserSession.UsuarioLogado.bt_permissaofuncionario == true)
                {
                    BtnFolhadepagamento.Enabled = false;
                    BtnFornecedor.Enabled = false;
                    BtnVendas.Enabled = false;
                    BtnFluxocaixa.Enabled = false;
                    btncompras.Enabled = false;
                    BtnFuncionarios.Enabled = false;

                   
                }
            }
        }



        
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Você quer mesmo sair?", "Michael Pop´s",
   MessageBoxButtons.YesNo, MessageBoxIcon.Question)
   == DialogResult.Yes)
            {
                fader.Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolhadePagamento tela = new FolhadePagamento();
            tela.Show();
            this.Hide();
            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Login tela = new Login();
            tela.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Fornecedor tela = new Fornecedor();
            tela.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Produto tela = new Produto();
            tela.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pedido tela = new Pedido();
            tela.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Cliente tela = new Cliente();
            tela.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            GASTOSS tela = new GASTOSS();
            tela.Show();
            this.Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Estoque tela = new Estoque();
            tela.Show();
            this.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {

            Fluxo_de_Caixa tela = new Fluxo_de_Caixa();
            tela.Show();
            this.Hide();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Funcionarios tela = new Funcionarios();
            tela.Show();
            this.Hide();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Cliente tela = new Cliente();
            tela.Show();
            this.Hide();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Sobre tela = new Sobre();
            tela.Show();
            this.Hide();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Historia tela = new Historia();
            tela.Show();
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CadastrarCompra tela = new CadastrarCompra();
            tela.Show();
            this.Hide();
        }

        private void fader_Tick(object sender, EventArgs e)
        {
            if (this.Opacity > 0.0)
            {
                this.Opacity -= 0.15;
            }
            else
            {
                fader.Stop();
                Application.Exit();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Email tela = new Email();
            tela.Show();
            this.Hide();
        }

    }
}
